// Copyright 2019 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * RDMAexchangeidcallbacks.h
 * Andrew Ensor
 * C API with range of callback function options for exchanging RDMA identifiers
 * PSN, QPN, GID, LID between local and remote
 *
*/

#ifndef RDMA_EXCHANGE_ID_CALLBACKS_H
#define RDMA_EXCHANGE_ID_CALLBACKS_H

#include <stdbool.h> // used for bool
#include <inttypes.h> // used for uint32_t etc types
#include <infiniband/verbs.h>
#include <unistd.h> // used for sleep

#include "RDMAlogger.h"


enum exchangeResult {EXCH_SUCCESS=0, EXCH_MISSING_FILENAME};

/**********************************************************************
 * Log the resulting local and obtained remote identifiers
 **********************************************************************/
void logIdentifiers(
    uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
    uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr);

/**********************************************************************
 * Exchanges the necessary RDMA configuration identifiers with those of
 * the remote RDMA peer, by printing the local values to the display
 * and prompting the user to enter the remote values
 * Returns EXCH_SUCCESS if identifier exchange has completed successfully
 **********************************************************************/
enum exchangeResult exchangeViaStdIO(bool isSendMode,
    uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
    uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr);

/**********************************************************************
 * Sets the identifier file name for using exchangeViaSharedFiles
 **********************************************************************/
void setIdentifierFileName(const char *fileName);

/**********************************************************************
 * Exchanges the necessary RDMA configuration identifiers with those of
 * the remote RDMA peer, by saving the local identifiers to a file and
 * reading the remote identifiers from another file
 * Note this function requires first setting the filename via setIdentifierFileName
 * Returns EXCH_SUCCESS if identifier exchange has completed successfully
 **********************************************************************/
enum exchangeResult exchangeViaSharedFiles(bool isSendMode,
    uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
    uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr);

#endif

// Copyright 2019 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * RDMAapi.h
 * Andrew Ensor
 * C API for setting up an RDMA connection and sending/receiving using RDMA UC
 *
*/

#ifndef RDMA_API_H
#define RDMA_API_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <infiniband/verbs.h>
#include <infiniband/umad.h>
#include <rdma/rdma_cma.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <inttypes.h>
#include <malloc.h>
#include <math.h>
#include <netdb.h>

#include "RDMAlogger.h"
#include "RDMAexchangeidcallbacks.h"
#include "RDMAmemorymanager.h"
#include "RDMAmetricreporter.h"

#define SUCCESS (0)
#define FAILURE (1)

#define MIN_WORK_REQUEST_ENQUEUE (0.85) /* minimum fraction of queue capacity to which to enqueue work requests */

enum runMode {RECV_MODE, SEND_MODE};

/**********************************************************************
 * Initialise libibverb data structures so have fork() protection
 * Note this has a performance hit, and is optional
 **********************************************************************/
int enableForkProtection();

/**********************************************************************
 * Convenience function that sleeps the current thread for the specified
 * number of microseconds
 * Note this function gets called by sendWorkRequests and receiveWorkRequests
 **********************************************************************/
void microSleep(long delayMicrosec);

/**********************************************************************
 * Allocate the RDMA resources, including RDMA context, event channel,
 * protection domain, receive and send queues and return them in the
 * pointer parameters
 **********************************************************************/
int allocateRDMAResources(char *rdmaDeviceName, uint8_t rdmaPort,
    uint32_t queueCapacity, uint32_t maxInlineDataSize,
    struct ibv_context **rdmaDeviceContextPtr,
    struct ibv_comp_channel **eventChannelPtr,
    struct ibv_pd **protectionDomainPtr,
    struct ibv_cq **receiveCompletionQueuePtr,
    struct ibv_cq **sendCompletionQueuePtr,
    struct ibv_qp **queuePairPtr);

/**********************************************************************
 * Sets up the RDMA connection and return its details in the
 * pointer parameters
 **********************************************************************/
int setupRDMAConnection(struct ibv_context *rdmaDeviceContext, uint8_t rdmaPort,
    uint16_t *localIdentifierPtr, union ibv_gid *gidAddressPtr, int *gidIndexPtr,
    enum ibv_mtu *mtuPtr);

/**********************************************************************
 * Convenience function that displays the specified number of bytes
 * from the start and end of each memory region in the memory buffer
 **********************************************************************/
void displayMemoryBuffer(struct ibv_mr **memoryRegions, uint32_t numMemoryRegions,
    int startBytes, int endBytes);

/**********************************************************************
 * Modifies the queue pair so that it is ready to receive
 * and possibly to also send
 **********************************************************************/
int modifyQueuePairReady(struct ibv_qp *queuePair, uint8_t rdmaPort, int gidIndex,
    enum runMode mode, int packetSequenceNumber,
    uint32_t remotePSN, uint32_t remoteQPN, union ibv_gid remoteGID, uint16_t remoteLID,
    enum ibv_mtu mtu);

/**********************************************************************
 * Registers memory regions with protection domain
 **********************************************************************/
int registerMemoryRegions(struct ibv_pd *protectionDomain, MemoryRegionManager* manager);

/**********************************************************************
 * Sends the contents of the memory regions as RDMA message work requests
 * where the parameters are provided in the struct sendWorkRequestParams
 **********************************************************************/
void *sendWorkRequests(void *ptr);

/**********************************************************************
 * Receives the RDMA work requests into the memory regions where the
 * parameters are provided in the struct receiveWorkRequestParams
 * Note if the work completions include immediate data then the receiver
 * checks that the immediate data values increment consecutively (or else
 * are reset using a smaller value than the previous immediate data value)
 **********************************************************************/
void *receiveWorkRequests(void *ptr);

/**********************************************************************
 * Deregister allocated memory regions and frees memoryRegions
 **********************************************************************/
void deregisterMemoryRegions(MemoryRegionManager* manager);

/**********************************************************************
 * Cleans up allocated RDMA resources
 **********************************************************************/
void cleanupRDMAResources(struct ibv_context *rdmaDeviceContext,
    struct ibv_comp_channel *eventChannel,
    struct ibv_pd *protectionDomain,
    struct ibv_cq *receiveCompletionQueue,
    struct ibv_cq *sendCompletionQueue,
    struct ibv_qp *queuePair);

/**********************************************************************
 * Convenience function to prepare the RDMA device and then send or receive RDMA
 * using allocated memory blocks and the default detected RDMA device, RDMA port, GID
 * Note if identifierExchangeFunction==NULL then default to using stdio for exchanging RDMA identifiers
 **********************************************************************/
int rdmaTransferMemoryBlocksWithDefaultDevice(enum runMode mode, MemoryRegionManager *manager, uint32_t messageDelayTime,
    enum exchangeResult (*identifierExchangeFunction)(bool isSendMode,
        uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
        uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr));

/**********************************************************************
 * Convenience function to prepare the RDMA device and then send or receive RDMA
 * using allocated memory blocks
 * Note if identifierExchangeFunction==NULL then default to using stdio for exchanging RDMA identifiers
 * Note if rdmaDeviceName==NULL then no preferred rdma device name to choose
 * Note if gidIndex = -1 then no preferred gid index to use
 **********************************************************************/
int rdmaTransferMemoryBlocks(enum runMode mode, MemoryRegionManager *manager, uint32_t messageDelayTime,
    enum exchangeResult (*identifierExchangeFunction)(bool isSendMode,
        uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
        uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr),
    char *rdmaDeviceName, uint8_t rdmaPort, int gidIndex, char *metricURL, uint32_t numMetricAveraging);

/**********************************************************************
 * Convenience function to wait until rdmaTransferMemoryBlocks has completed
 **********************************************************************/
int waitForRdmaTransferCompletion(MemoryRegionManager *manager);

#endif

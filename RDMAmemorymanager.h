// Copyright 2019 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * RDMAmemorymanager.h
 * Andrew Ensor
 * C API for tracking memory region usage when using RDMA
 *
*/

#ifndef RDMA_MEMORY_MANAGER_H
#define RDMA_MEMORY_MANAGER_H

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <malloc.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h> // required for _SC_PAGESIZE

#include "RDMAlogger.h"

/* The struct MemoryRegionManager is used to hold any memory-related information about the RDMA transfers
 * Note this struct might be accessed by multiple threads so its variables should never be accessed directly,
 * but instead via the provided functions that ensure access is suitably synchronised
 */
typedef struct MemoryRegionManager
{
    void **memoryBlocks;
    uint32_t messageSize; /* size in bytes of single RDMA message */
    uint32_t numMemoryRegions; /* number of memory regions to be managed, where each region holds one memory block */
    uint32_t numContiguousMessages; /* number of contiguous messages to hold in each memory region */
    uint64_t numTotalMessages; /* total number of messages to send or receive, if 0 then default to numMemoryBlocks*numContiguousMessages */
    uint64_t memoryBlockSize; /* calculated to be messageSize * numContiguousMessages */
    struct ibv_mr** memoryRegions;
    bool isMonitoringRegions; /* whether the manager actively monitors whether each region is populated during sending or receiving */
    bool *populated; /* whether each memory region is currently populated with data */
    bool *enqueued; /* whether each memory region is currently enqueued for RDMA send or receive */
    uint64_t *messageTransferred; /* ordinal for message most recently transferred in each of the contiguous memory locations, or UINT64_MAX if none */
    pthread_mutex_t mutex; /* mutex for synchronising access to populated, enqueued, messageTransferred from multiple threads */
    pthread_t enqueueWorkRequestsThread; /* pthread used to asynchronously send or receive work requests */
} MemoryRegionManager;

static const uint64_t NO_MESSAGE_ORDINAL = UINT64_MAX;

/**********************************************************************
 * Convenience function that allocates page aligned memory blocks suitable
 * for passing to a MemoryRegionManager
 * Should free the memory blocks when done via freeMemoryBlocks
 * Note that contents of numMemoryBlocks will be modified if not all
 * the requested memory blocks could be allocated
 **********************************************************************/
void** allocateMemoryBlocks(uint64_t memoryBlockSize, uint32_t *numMemoryBlocks);

/**********************************************************************
 * Creates a MemoryRegionManager struct to report on status of a
 * collection of memory regions used for RDMA (sending or receiving)
 **********************************************************************/
MemoryRegionManager* createMemoryRegionManager(void **memoryBlocks,
    uint32_t messageSize, uint32_t numMemoryBlocks, uint32_t numContiguousMessages,
    uint64_t numTotalMessages, bool isMonitoringRegions);

/**********************************************************************
 * Convenience function that reads all the data into the MemoryRegionManager
 * from dataFileName.0, dataFileName.1, ...
 * Note first int in each file is assumed to be the number of 8 byte values
 * in the file which gets discarded
 * Returns true if the files were successfully read into the memory blocks
 **********************************************************************/
bool readFilesIntoMemoryBlocks(MemoryRegionManager *manager, const char *dataFileName);

/**********************************************************************
 * Convenience function that writes all the data from the MemoryRegionManager
 * to dataFileName.0, dataFileName.1, ...
 * Note first int in each file is the number of 8 byte values in memory block
 * Returns true if the files were successfully written from the memory blocks
 **********************************************************************/
bool writeFilesFromMemoryBlocks(MemoryRegionManager *manager, const char *dataFileName);

/**********************************************************************
 * Convenience function that displays the specified number of bytes
 * from the start and end of each memory block
 **********************************************************************/
void displayMemoryBlocks(MemoryRegionManager *manager, uint32_t startBytes, uint32_t endBytes);

/**********************************************************************
 * Sets the populated status of all memory region
 **********************************************************************/
void setAllMemoryRegionsPopulated(MemoryRegionManager *manager, bool status);

/**********************************************************************
 * Gets the populated status of all specified memory regions
 * Note this function presumes populatedRegions is sufficiently large to
 * hold results
 **********************************************************************/
void getAllMemoryRegionsPopulated(MemoryRegionManager *manager, bool *populatedRegions);

/**********************************************************************
 * Sets the populated status of a specified memory region
 **********************************************************************/
void setMemoryRegionPopulated(MemoryRegionManager *manager, uint32_t regionIndex, bool status);

/**********************************************************************
 * Gets the populated status of a specified memory region
 **********************************************************************/
bool getMemoryRegionPopulated(MemoryRegionManager *manager, uint32_t regionIndex);

/**********************************************************************
 * Sets the enqueued status of all memory region
 **********************************************************************/
void setAllMemoryRegionsEnqueued(MemoryRegionManager *manager, bool status);

/**********************************************************************
 * Gets the enqueued status of all specified memory regions
 * Note this function presumes enqueuedRegions is sufficiently large to
 * hold results
 **********************************************************************/
void getAllMemoryRegionsEnqueued(MemoryRegionManager *manager, bool *enqueuedRegions);

/**********************************************************************
 * Sets the enqueued status of a specified memory region
 **********************************************************************/
void setMemoryRegionEnqueued(MemoryRegionManager *manager, uint32_t regionIndex, bool status);

/**********************************************************************
 * Gets the enqueued status of a specified memory region
 **********************************************************************/
bool getMemoryRegionEnqueued(MemoryRegionManager *manager, uint32_t regionIndex);

/**********************************************************************
 * Sets the transferred message ordinal for specified memory location
 **********************************************************************/
void setMessageTransferred(MemoryRegionManager *manager, uint32_t regionIndex, uint32_t contiguousIndex, uint64_t messageOrdinal);

/**********************************************************************
 * Gets the transferred message ordinal for specified memory location
 **********************************************************************/
uint64_t getMessageTransferred(MemoryRegionManager *manager, uint32_t regionIndex, uint32_t contiguousIndex);

/**********************************************************************
 * Destroys a MemoryRegionManager struct that was used for reporting
 **********************************************************************/
void destroyMemoryRegionManager(MemoryRegionManager *manager);

/**********************************************************************
 * Convenience function that frees allocated memory blocks created by allocateMemoryBlocks
 **********************************************************************/
void freeMemoryBlocks(void **memoryBlocks, uint32_t numMemoryBlocks);

#endif

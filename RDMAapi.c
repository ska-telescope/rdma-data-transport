// Copyright 2019 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * RDMAapi.c
 * Andrew Ensor
 * C API for setting up an RDMA connection and sending/receiving using RDMA UC
 *
*/

#include "RDMAapi.h"

/**********************************************************************
 * Initialise libibverb data structures so have fork() protection
 * Note this has a performance hit, and is optional
 **********************************************************************/
int enableForkProtection()
{
    int rc;
    rc = ibv_fork_init();
    if (rc != SUCCESS)
    {
        logger(LOG_WARNING, "Unable to enable fork protection, error code %d", rc);
        return FAILURE;
    }
    else
        logger(LOG_INFO, "Fork protection set");
    return SUCCESS;
}


/**********************************************************************
 * Convenience function that sleeps the current thread for the specified
 * number of microseconds
 * Note this function gets called by sendWorkRequests and receiveWorkRequests
 **********************************************************************/
void microSleep(long delayMicrosec)
{
    /* include sleep delay if specified */
    if (delayMicrosec > 0)
    {
        struct timespec ts;
        ts.tv_sec = (int)(delayMicrosec / 1000000L);
        ts.tv_nsec = (delayMicrosec - ((long)ts.tv_sec)*1000000L) * 1000L;
        if (nanosleep(&ts, NULL) < 0)
            logger(LOG_INFO, "Sleep delay interrupted");
    }
}


/**********************************************************************
 * Returns and opens a specified RDMA device if its name is specified
 * or else returns and opens first RDMA device found
 * Note this function gets called by allocateRDMAResources so does not
 * need to be called directly
 * Returns null if no device found
 **********************************************************************/
struct ibv_context *openRdmaDevice(const char *deviceName)
{
    int numDevices;
    struct ibv_device **deviceList;
    struct ibv_device *device = NULL;
    struct ibv_context *deviceContext = NULL;

    /* get the list of RDMA devices */
    deviceList = ibv_get_device_list(&numDevices);
    logger(LOG_DEBUG, "Obtained device list with %d devices", numDevices);
    for (int i=0; i<numDevices; i++)
    {
        logger(LOG_INFO, "RDMA device found: %s", ibv_get_device_name(deviceList[i]));
    }
    if (numDevices <= 0)
    {
        logger(LOG_CRIT, "Failed to get RDMA device list");
        return NULL;
    }
    if (deviceName == NULL)
    {
        /* no preferred device specified so just return the first one found */
        device = deviceList[0];
    }
    else
    {
        /* search for the specified device in the device list */
        for (int i=0; i<numDevices; i++)
        {
            /* get the next device in the list */
            device = deviceList[i];
            if (!device)
            {
                logger(LOG_WARNING, "Null RDMA device found in list");
                continue;
            }
            else
            {
                logger(LOG_DEBUG, "RDMA device found %s", ibv_get_device_name(device));
                if (strcmp(ibv_get_device_name(device), deviceName) == SUCCESS)
                {
                    /* device found */
                    break;
                }
            }
        }
    }
    if (device == NULL)
    {
        logger(LOG_CRIT, "Null RDMA device found");
        return NULL;
    }        

    /* open the found device */
    deviceContext = ibv_open_device(device);
    if (!deviceContext)
    {
        logger(LOG_CRIT, "Unable to open RDMA device to obtain context");
        return NULL;
    }
    
    enum ibv_transport_type rdmaTransportType = deviceContext->device->transport_type;
    if (rdmaTransportType == IBV_TRANSPORT_UNKNOWN)
    {
        logger(LOG_WARNING, "Warning transport type is unknown");
    }

    /* free device list only after opening found device */
    ibv_free_device_list(deviceList);
    deviceList = NULL;
    device = NULL;

    return deviceContext;
}


const char *portStates[6] = {"Nop", "Port Down", "Port Initializing",
  "Port Armed", "Port Active", "Port Active Deferred"};


/**********************************************************************
 * Sets the link layer for the RDMA connection
 * Note this function gets called by allocateRDMAResources so does not
 * need to be called directly
 **********************************************************************/
int checkLink(struct ibv_context *deviceContext, uint8_t port)
{
    struct ibv_port_attr portAttributes;
    memset(&portAttributes, 0, sizeof portAttributes);

    if (ibv_query_port(deviceContext, port, &portAttributes))
    {
        logger(LOG_CRIT, "Unable to query RDMA port attributes");
        return FAILURE;
    }
    if (portAttributes.state != IBV_PORT_ACTIVE)
    {
        logger(LOG_CRIT, "RDMA port %d is not active, is %s",
            port, portStates[portAttributes.state]);
        return FAILURE;
    }

    uint8_t link = portAttributes.link_layer;
    if (link!=IBV_LINK_LAYER_ETHERNET && link!=IBV_LINK_LAYER_INFINIBAND)
    {
        logger(LOG_CRIT, "Unknown link layer protocol");
        return FAILURE;
    }

    /* query device attributes, which can be used to find vendor and model */ 
    struct ibv_device_attr deviceAttributes;
    memset(&deviceAttributes, 0, sizeof deviceAttributes);
    int supportsContiguousPages;
    int errorCode = ibv_query_device(deviceContext, &deviceAttributes);
    if (errorCode != 0)
    {
        logger(LOG_ERR, "Error %d obtaining device attributes", errorCode);
        supportsContiguousPages = FAILURE;
    }
    else
        supportsContiguousPages = (deviceAttributes.device_cap_flags & (1<<23))
            ? SUCCESS : FAILURE;
    if (supportsContiguousPages == SUCCESS)
        logger(LOG_INFO, "Contiguous pages are supported");
    else
        logger(LOG_INFO, "Contiguous pages are not supported");

    return SUCCESS;
}


/**********************************************************************
 * Create a completion queue pair
 * Note this function gets called by allocateRDMAResources so does not
 * need to be called directly
 **********************************************************************/
struct ibv_qp *createQueuePair(struct ibv_pd *protectionDomain,
    struct ibv_cq *sendCompletionQueue, struct ibv_cq *receiveCompletionQueue,
    uint8_t rdmaPort, uint32_t queueCapacity, uint32_t maxInlineDataSize)
{
    struct ibv_qp_init_attr initAttribs;
    memset(&initAttribs, 0, sizeof initAttribs);
    initAttribs.send_cq = sendCompletionQueue;
    initAttribs.recv_cq = receiveCompletionQueue;
    initAttribs.cap.max_send_wr = queueCapacity;
    initAttribs.cap.max_send_sge = 1;
    initAttribs.cap.max_inline_data = maxInlineDataSize;
    initAttribs.srq = NULL;
    initAttribs.cap.max_recv_wr = queueCapacity;
    initAttribs.cap.max_recv_sge = 1; /* note this would need ot be increased to support receiving multiple sge per work requests */
    initAttribs.sq_sig_all = 1; /* note signalling all work completions on sender appears to help receiver lose fewer messages */
    initAttribs.qp_type = IBV_QPT_UC;
    struct ibv_qp* queuePair = ibv_create_qp(protectionDomain, &initAttribs);
    if (queuePair == NULL && errno == ENOMEM)
    {
        logger(LOG_CRIT, "Requested queue pair size is too large");
        return NULL;
    }

    struct ibv_qp_attr attributes;
    memset(&attributes, 0, sizeof attributes);
    attributes.qp_state = IBV_QPS_INIT;
    attributes.pkey_index = 0;
    attributes.port_num = rdmaPort;
    attributes.qp_access_flags = IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE;
        /***** NOTE not setting IBV_ACCESS_REMOTE_READ *****/
    int flags = IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS;
    if (ibv_modify_qp(queuePair, &attributes, flags) != SUCCESS)
    {
        logger(LOG_CRIT, "Unable to modify queue pair to INIT");
    }

    return queuePair;
} 


/**********************************************************************
 * Allocate the RDMA resources, including RDMA context, event channel,
 * protection domain, receive and send queues and return them in the
 * pointer parameters
 **********************************************************************/
int allocateRDMAResources(char *rdmaDeviceName, uint8_t rdmaPort,
    uint32_t queueCapacity, uint32_t maxInlineDataSize,
    struct ibv_context **rdmaDeviceContextPtr,
    struct ibv_comp_channel **eventChannelPtr,
    struct ibv_pd **protectionDomainPtr,
    struct ibv_cq **receiveCompletionQueuePtr,
    struct ibv_cq **sendCompletionQueuePtr,
    struct ibv_qp **queuePairPtr)
{
    /* find and open the RDMA-capable device */
    *rdmaDeviceContextPtr = openRdmaDevice(rdmaDeviceName);
    if (*rdmaDeviceContextPtr == NULL)
    {
        logger(LOG_CRIT, "Cannot find the RDMA-capable device");
        return FAILURE;
    }
    else
        logger(LOG_DEBUG, "RDMA device opened");

    /* check the link for the specified port */
    /* note not considering dual-port mode here for a second GID */
    if (checkLink(*rdmaDeviceContextPtr, rdmaPort) != SUCCESS)
    {
        logger(LOG_CRIT, "Link failure during checking");
        return FAILURE;
    }
    else
        logger(LOG_DEBUG, "Link checked for RDMA port");

    /* create an event completion channel */
    *eventChannelPtr = ibv_create_comp_channel(*rdmaDeviceContextPtr);
    if (*eventChannelPtr == NULL)
    {
        logger(LOG_CRIT, "Event completion channel failure during creation");
        return FAILURE;
    }
    else
        logger(LOG_DEBUG, "Event completion channel created");

    /* allocate the protection domain */
    *protectionDomainPtr = ibv_alloc_pd(*rdmaDeviceContextPtr);
    if (*protectionDomainPtr == NULL)
    {
        logger(LOG_CRIT, "Unable to create protection domain");
        return FAILURE;
    }
    else
        logger(LOG_DEBUG, "Protection domain allocated");

    /* create the completion queues */
    *receiveCompletionQueuePtr = ibv_create_cq(*rdmaDeviceContextPtr, queueCapacity, NULL, *eventChannelPtr, 0);
    if (*receiveCompletionQueuePtr == NULL)
    {
        logger(LOG_CRIT, "Unable to create receive completion queue");
        return FAILURE;
    }
    else
        logger(LOG_DEBUG, "Receive completion queue created");
    *sendCompletionQueuePtr = ibv_create_cq(*rdmaDeviceContextPtr, queueCapacity, NULL, *eventChannelPtr, 0);
    if (*sendCompletionQueuePtr == NULL)
    {
        logger(LOG_ERR, "Unable to create send completion queue");
        *sendCompletionQueuePtr = *receiveCompletionQueuePtr; /* try using same queue for both send and receive */
    }
    else
        logger(LOG_DEBUG, "Send completion queue created");

    /* create the completion queue pair */
    *queuePairPtr = createQueuePair(*protectionDomainPtr, *sendCompletionQueuePtr,
        *receiveCompletionQueuePtr, rdmaPort, queueCapacity, maxInlineDataSize);
    if (*queuePairPtr == NULL)
    {
        logger(LOG_CRIT, "Unable to create queue pair");
        return FAILURE;
    }
    return SUCCESS;
}


/**********************************************************************
 * Sets up the RDMA connection and return its details in the
 * pointer parameters
 **********************************************************************/
int setupRDMAConnection(struct ibv_context *rdmaDeviceContext, uint8_t rdmaPort,
    uint16_t *localIdentifierPtr, union ibv_gid *gidAddressPtr, int *gidIndexPtr,
    enum ibv_mtu *mtuPtr)
{
    struct ibv_port_attr portAttributes;
    if (ibv_query_port(rdmaDeviceContext, rdmaPort, &portAttributes) != SUCCESS)
    {
        logger(LOG_CRIT, "Unable to query RDMA port attributes");
        return FAILURE;
    }
    if (*gidIndexPtr < 0)
    {
        /* find best GID to use based on ip version if no preferred gid index specified */
        logger(LOG_DEBUG, "GID table has %d entries", portAttributes.gid_tbl_len);
        int ipVersionPreferred = 6; /* default to IPv6 instead of IPv4 */
        union ibv_gid currentGID;
        union ibv_gid bestGID;
        int bestGIDIndex = -1;
        int bestIsIPv4 = -1;
        for (int i=0; i<portAttributes.gid_tbl_len; i++)
        {
            /* look up the GID address */
            if (ibv_query_gid(rdmaDeviceContext, rdmaPort, i, &currentGID) != SUCCESS)
            {
                logger(LOG_ERR, "Unable to query RDMA port GID table at index %d", i);
                continue;
            }
            /* determine whether IP address mapped to v4 */
            struct in6_addr *address = (struct in6_addr *)currentGID.raw;
            int isIPv4 = ((address->s6_addr32[0] | address->s6_addr32[1]) |
                (address->s6_addr32[2] ^ htonl(0x0000ffff))) == 0UL ||
	        /* IPv4 encoded multicast addresses */
                (address->s6_addr32[0] == htonl(0xff0e0000) &&
                ((address->s6_addr32[1] |
                (address->s6_addr32[2] ^ htonl(0x0000ffff))) == 0UL));
            logger(LOG_DEBUG, "GID table entry %d and %s", i, (isIPv4!=0) ? "IPv4" : "IPv6"); 
            if (i==0 || (isIPv4 && (ipVersionPreferred==4) && !bestIsIPv4) ||
                (!isIPv4 && (ipVersionPreferred==6) && bestIsIPv4))
            {
                bestGID = currentGID;
                bestGIDIndex = i;
                bestIsIPv4 = isIPv4;
            }
        }
        *gidAddressPtr = bestGID;
        *gidIndexPtr = bestGIDIndex;
    }
    else
    {
        /* preferred gid index has been specified */
        /* look up the GID address */
        if (ibv_query_gid(rdmaDeviceContext, rdmaPort, *gidIndexPtr, gidAddressPtr) != SUCCESS)
        {
            logger(LOG_CRIT, "Unable to query RDMA port GID table for preferred index %d", *gidIndexPtr);
            return FAILURE;
        }
    }
    /* get local ID */
    memset(&portAttributes, 0, sizeof portAttributes); /* clear portAttributes struct */
    if (ibv_query_port(rdmaDeviceContext, rdmaPort, &portAttributes) != SUCCESS)
    {
        logger(LOG_ERR, "Unable to query local RDMA port attributes");
        *localIdentifierPtr = 0;
        *mtuPtr = IBV_MTU_4096;
    }
    else
    {
        *localIdentifierPtr = portAttributes.lid;
        *mtuPtr = portAttributes.active_mtu;
        logger(LOG_DEBUG, "RDMA port queried to get LID %" PRIu16 " and MTU enum %d", *localIdentifierPtr, *mtuPtr);
    }
    
    return SUCCESS;
}


/**********************************************************************
 * Modifies the queue pair so that it is ready to receive
 * and possibly to also send
 **********************************************************************/
int modifyQueuePairReady(struct ibv_qp *queuePair, uint8_t rdmaPort, int gidIndex,
    enum runMode mode, int packetSequenceNumber,
    uint32_t remotePSN, uint32_t remoteQPN, union ibv_gid remoteGID, uint16_t remoteLID,
    enum ibv_mtu mtu)
{
    struct ibv_qp_attr attributes;
    memset(&attributes, 0, sizeof attributes);
    int flags = IBV_QP_STATE | IBV_QP_AV | IBV_QP_PATH_MTU | IBV_QP_DEST_QPN | IBV_QP_RQ_PSN;
    attributes.qp_state = IBV_QPS_RTR;
    attributes.ah_attr.src_path_bits = 0;
    attributes.ah_attr.port_num = rdmaPort;
    attributes.ah_attr.sl = 0; /* default service level */
    attributes.ah_attr.is_global = 1;
    attributes.ah_attr.grh.dgid = remoteGID;
    attributes.ah_attr.grh.sgid_index = gidIndex;
    attributes.ah_attr.grh.hop_limit = 0xFF; /***** NOTE set to 1 in Will's demo, NOTE no flow_label=0 */
    attributes.ah_attr.grh.traffic_class = 0;
    attributes.ah_attr.dlid = remoteLID;
    attributes.path_mtu = mtu; /* note ConnectX3-Pro and ConnectX-3 have bug with IBV_MTU_4096 */
    attributes.dest_qp_num = remoteQPN;
    attributes.rq_psn = remotePSN;
    if (ibv_modify_qp(queuePair, &attributes, flags) != SUCCESS)
    {
        logger(LOG_CRIT, "Unable to modify queue pair to be ready to receive %d",errno);
        return FAILURE;
    }
    if (mode == SEND_MODE)
    {
        flags = IBV_QP_STATE | IBV_QP_SQ_PSN;
        attributes.qp_state = IBV_QPS_RTS;
        attributes.sq_psn = packetSequenceNumber;
        if (ibv_modify_qp(queuePair, &attributes, flags) != SUCCESS)
        {
            logger(LOG_CRIT, "Unable to modify queue pair to be ready to send %d",errno);
            return FAILURE;
        }
    }
    return SUCCESS;
}


/**********************************************************************
 * Registers memory regions with protection domain
 **********************************************************************/
int registerMemoryRegions(struct ibv_pd *protectionDomain, MemoryRegionManager* manager)
{
    manager->memoryRegions = (struct ibv_mr**)malloc(sizeof(struct ibv_mr *) * manager->numMemoryRegions);
    if (manager->memoryRegions == NULL)
    {
        logger(LOG_ERR, "Unable to create memory region array");
        return FAILURE;
    }
    int flags = IBV_ACCESS_LOCAL_WRITE;
        /* NOTE not setting IBV_ACCESS_REMOTE_READ | IBV_ACCESS_REMOTE_WRITE */
    for (uint32_t regionIndex=0; regionIndex<manager->numMemoryRegions; regionIndex++)
    {
        /* register memory to pin it with the RDMA device */
        struct ibv_mr *memoryRegion = NULL;
        memoryRegion = ibv_reg_mr(protectionDomain, manager->memoryBlocks[regionIndex],
            manager->memoryBlockSize, flags);
        if (memoryRegion == NULL)
        {
            logger(LOG_ERR, "Unable to register memory region number %" PRIu32, regionIndex);
        }
        manager->memoryRegions[regionIndex] = memoryRegion;
    }
    return SUCCESS;
}


/**********************************************************************
 * Waits until a completion event is received on the eventChannel
 * then acknowledge it and reset it to receive another notification that
 * may come in future on that completion queue
 * Note this function gets called by sendWorkRequests and receiveWorkRequests
 * so does not need to be called directly
 **********************************************************************/
int waitForCompletionQueueEvent(struct ibv_comp_channel *eventChannel)
{
    /* wait until get completion queue event */
    struct ibv_cq *eventCompletionQueue;
    void *eventContext;
    if (ibv_get_cq_event(eventChannel, &eventCompletionQueue, &eventContext) != SUCCESS)
    {
        logger(LOG_ERR, "Error while waiting for completion queue event");
        return FAILURE;
    }
    /* assert: eventCompletionQueue is either receiveCompletionQueue or sendCompletionQueue */
    /* assert: eventContext is rdmaDeviceContext */
    /* acknowledge the one completion queue event */
    ibv_ack_cq_events(eventCompletionQueue, 1);
    /* request notification for next completion event on the completion queue */
    if (ibv_req_notify_cq(eventCompletionQueue, 0) != SUCCESS)
    {
        logger(LOG_WARNING, "Error while receiver requesting completion notification");
    }
    return SUCCESS;
}


/**********************************************************************
 * struct that holds parameters for sendWorkRequests
 **********************************************************************/
struct sendWorkRequestParams
{
    MemoryRegionManager* manager;
    struct ibv_qp *queuePair;
    struct ibv_comp_channel *eventChannel;
    uint32_t messageDelayTime;
    uint32_t maxInlineDataSize;
    struct ibv_cq *sendCompletionQueue;
    uint32_t queueCapacity;
    char *metricURL;
    uint32_t numMetricAveraging;
    struct ibv_context *rdmaDeviceContext; // required for cleanup once transfers completed
    struct ibv_pd *protectionDomain; // required for cleanup once transfers completed
    struct ibv_cq *receiveCompletionQueue; // required for cleanup once transfers completed
};


/**********************************************************************
 * Sends the contents of the memory regions as RDMA message work requests
 * where the parameters are provided in the struct sendWorkRequestParams
 * When done, also deregisters memory regions, cleans up RDMA resources and frees ptr
 **********************************************************************/
void *sendWorkRequests(void *ptr)
{
    struct sendWorkRequestParams *params = (struct sendWorkRequestParams *)ptr;
    MemoryRegionManager* manager = params->manager;

    /* prepare scatter/gather array specifying buffers to read from or write into */
    /* note this implementation uses only one sgItem per work request but chains together */
    /* multiple work requests when numContiguousMessages > 1 */
    /* Alternatively, could instead use multiple sge, but then need to adjust queue pair specs */
    struct ibv_sge sgItems[manager->numMemoryRegions][manager->numContiguousMessages];
    memset(sgItems, 0, sizeof(struct ibv_sge[manager->numMemoryRegions][manager->numContiguousMessages]));
    int sgListLength = 1; /* only one sgItem per work request */

    /* prepare array of work requests across all memory regions for the send queue */
    struct ibv_send_wr workRequests[manager->numMemoryRegions][manager->numContiguousMessages];
    memset(workRequests, 0, sizeof(struct ibv_send_wr[manager->numMemoryRegions][manager->numContiguousMessages]));
    for (uint32_t regionIndex=0; regionIndex<manager->numMemoryRegions; regionIndex++)
    {
        for (uint32_t i=0; i<manager->numContiguousMessages; i++)
        {
            /* prepare one sgItem to send */
            sgItems[regionIndex][i].addr = (uint64_t)(manager->memoryRegions[regionIndex]->addr + i*manager->messageSize);
            sgItems[regionIndex][i].length = manager->messageSize;
            sgItems[regionIndex][i].lkey = manager->memoryRegions[regionIndex]->lkey;

            /* prepare one workRequest with this sgItem */
            workRequests[regionIndex][i].wr_id = (uint64_t)(regionIndex*manager->numContiguousMessages + i); /* gives memory region and location */
            workRequests[regionIndex][i].sg_list = &sgItems[regionIndex][i];
            workRequests[regionIndex][i].num_sge = sgListLength;
            workRequests[regionIndex][i].opcode = IBV_WR_SEND_WITH_IMM;
            if (manager->messageSize <= params->maxInlineDataSize)
                workRequests[regionIndex][i].send_flags |= IBV_SEND_INLINE;
            /* optional immediate data sent along with payload, set to be incrementing values with zero first as reset */
            workRequests[regionIndex][i].imm_data = (uint32_t)0; /* set appropriately prior to each ibv_post_send */
            if (i == manager->numContiguousMessages-1)
            {
                workRequests[regionIndex][i].next = NULL; /* note can chain multiple workRequest together for performance */
                workRequests[regionIndex][i].send_flags = 0; // IBV_SEND_SIGNALED; /* include signal with last work request */
            }
            else
            {
                workRequests[regionIndex][i].next = &workRequests[regionIndex][i+1]; /* chain multiple workRequest together for performance */
            }
        }
    }
    /* note other workRequest.rdma/atomic/ud struct are unused for UC */

    /* setup for loop enqueueing blocks of work requests and polling for blocks of work completions */
    uint32_t minWorkRequestEnqueue = (uint32_t) ceil(params->queueCapacity * MIN_WORK_REQUEST_ENQUEUE);
    uint32_t maxWorkRequestDequeue = params->queueCapacity; /* try to completely drain queue of any completed work requests */
    struct ibv_send_wr *badWorkRequest = NULL;
    struct ibv_wc *workCompletions = NULL;
    workCompletions = (struct ibv_wc*)malloc(sizeof(struct ibv_wc) * maxWorkRequestDequeue);
    if (workCompletions == NULL)
    {
        logger(LOG_ERR, "Sender unable to allocate desired memory for work completions");
        return NULL;
    }
    setAllMemoryRegionsEnqueued(manager, false);
    if (params->metricURL != NULL)
    {
        initialiseMetricReporter();
    }
    uint32_t currentQueueLoading = 0; /* no work requests initially in queue */
    uint64_t numWorkRequestsEnqueued = 0;
    uint32_t regionIndex = 0;
    uint64_t numWorkRequestCompletions = 0;

    /* initialise sender metrics */
    uint64_t metricMessagesTransferred = 0;
    clock_t metricStartClockTime = clock();
    struct timeval metricStartWallTime;
    gettimeofday(&metricStartWallTime, NULL);

    /* loop enqueueing blocks of work requests and polling for blocks of work completions */
    /* note there is only one work completion signalled for each block of numContiguousMessages */
    /* that sit together in a workRequest linked list */
    while (numWorkRequestCompletions < manager->numTotalMessages)
    {
        /* post block of send work requests to sufficiently full work request queue */
        while ((numWorkRequestsEnqueued < manager->numTotalMessages) && (currentQueueLoading < minWorkRequestEnqueue))
        {
            /* provide incrementing immediate data sent along with payload */
            for (uint32_t i=0; i<manager->numContiguousMessages; i++)
            {
                workRequests[regionIndex][i].imm_data = htonl(numWorkRequestsEnqueued+i);
            }
            if (ibv_post_send(params->queuePair, &workRequests[regionIndex][0], &badWorkRequest) != SUCCESS)
            {
                logger(LOG_ERR, "Unable to post send request with error %d", errno);
            }
            else
            {
                logger(LOG_INFO, "Sender has posted work requests for region %" PRIu32, regionIndex);
                /* check that the memory region is already populated with data */
                if (manager->isMonitoringRegions && !getMemoryRegionPopulated(manager, regionIndex))
                {
                    logger(LOG_WARNING, "Memory region %" PRIu32 " has been enqueued for sending"
                        " without first being populated while enqueing work request %" PRIu64,
                        regionIndex, numWorkRequestsEnqueued);
                }
                currentQueueLoading += manager->numContiguousMessages;
                numWorkRequestsEnqueued += manager->numContiguousMessages;
                setMemoryRegionEnqueued(manager, regionIndex, true);
                regionIndex++;
                if (regionIndex >= manager->numMemoryRegions)
                    regionIndex = 0;
                microSleep(params->messageDelayTime);
            }
        }

        /* poll for block of work completions */
        logger(LOG_INFO, "Sender waiting for completion %" PRIu64 " of %" PRIu64, numWorkRequestCompletions, manager->numTotalMessages);
        if (waitForCompletionQueueEvent(params->eventChannel) != SUCCESS)
        {
            logger(LOG_ERR, "Sender unable to wait for completion notification");
        }

        /* poll the completion queue for work completions */
        /* NOTE perftest demo does not poll for number of workCompletions on sender */
        memset(workCompletions, 0, sizeof(struct ibv_wc) * maxWorkRequestDequeue);
        int numCompletionsFound = 0;
        numCompletionsFound = ibv_poll_cq(params->sendCompletionQueue, maxWorkRequestDequeue, workCompletions);
        if (numCompletionsFound < 0)
        {
            logger(LOG_WARNING, "Sender failed to poll for work completions"); 
        }
        else
        {
            logger(LOG_INFO, "Sender has polled %d work completions", numCompletionsFound);
            for (int wcIndex=0; wcIndex<numCompletionsFound; wcIndex++)
            {
                struct ibv_wc workCompletion = workCompletions[wcIndex];
                if (workCompletion.status != IBV_WC_SUCCESS)
                {
                    logger(LOG_ERR,
                        "Sender work completion error with status:%d, id:%" PRIu64 ", imm_data:%" PRIu32 ", syndrome:0x%x",
                        workCompletion.status, workCompletion.wr_id, workCompletion.imm_data, workCompletion.vendor_err);
                }
                else
                {
                    /* note imm_data seems to not appear in sender's workCompletion */
                    uint32_t transferredRegionIndex = (uint32_t)(workCompletion.wr_id / manager->numContiguousMessages);
                    uint32_t transferredContiguousIndex = (uint32_t)(workCompletion.wr_id % manager->numContiguousMessages);
                    setMemoryRegionEnqueued(manager, transferredRegionIndex, false);
                    /* set memory region to be unpopulated so can be repopulated with further data */
                    setMemoryRegionPopulated(manager, transferredRegionIndex, false);
                    setMessageTransferred(manager, transferredRegionIndex, transferredContiguousIndex, numWorkRequestCompletions);
                    logger(LOG_DEBUG,
                        "Sender work completion %" PRIu64 " success status with work request id %" PRIu64,
                        numWorkRequestCompletions, workCompletion.wr_id);
                }
                currentQueueLoading--;
                numWorkRequestCompletions++;
                metricMessagesTransferred++;
            }
        }
        /* update sender metrics, including partial metrics for last iteration of loop */
        if ((metricMessagesTransferred >= params->numMetricAveraging) || (numWorkRequestCompletions >= manager->numTotalMessages))
        {
            int queueUtilisation = (int)(100*currentQueueLoading/params->queueCapacity);
            logger(LOG_NOTICE, "Sender queue currently at %d%% capacity", queueUtilisation);
            uint64_t durationClockMicroSec = (uint64_t)((clock() - metricStartClockTime) * 1000000L / CLOCKS_PER_SEC);
            struct timeval endWallTime;
            gettimeofday(&endWallTime, NULL);
            uint64_t durationWallMicroSec = (endWallTime.tv_sec - metricStartWallTime.tv_sec) * 1000000L
                + (endWallTime.tv_usec-metricStartWallTime.tv_usec);
            int cpuUtilisation = (int)round(100.0*durationClockMicroSec/durationWallMicroSec); /* only approx measure */
            uint64_t metricBytesTransferred = manager->messageSize * metricMessagesTransferred;
            double bandwidth = 8.0E-3 * metricBytesTransferred / durationWallMicroSec; /* Gbps */
            logger(LOG_NOTICE, "Sender wall time duration is %" PRIu64 " milliseconds "
                "and CPU clock time duration is %" PRIu64 " milliseconds (%d%% utilisation)",
                durationWallMicroSec/1000, durationClockMicroSec/1000, cpuUtilisation);
            logger(LOG_NOTICE, "Sender bandwidth is %.2f Gbps", bandwidth);
            uint64_t millisecondsSinceEpoch = endWallTime.tv_sec*1000L + endWallTime.tv_usec/1000;
            if (params->metricURL != NULL)
            {
                pushMetrics("RDMAsender", params->metricURL, queueUtilisation, cpuUtilisation, bandwidth,
                    metricMessagesTransferred, 0, millisecondsSinceEpoch, manager);
            }
            /* reset sender metrics */
            metricMessagesTransferred = 0;
            metricStartClockTime = clock();
            gettimeofday(&metricStartWallTime, NULL);
        }
    }

    if (params->metricURL != NULL)
    {
        cleanupMetricReporter();
    }
    free(workCompletions);

    /* clean up allocated resources */
    deregisterMemoryRegions(manager);
    cleanupRDMAResources(params->rdmaDeviceContext, params->eventChannel, params->protectionDomain,
        params->receiveCompletionQueue, params->sendCompletionQueue, params->queuePair);
    free(params); // free malloc in rdmaTransferMemoryBlocks
    return NULL;
}


/**********************************************************************
 * struct that holds parameters for receiveWorkRequests
 **********************************************************************/
struct receiveWorkRequestParams
{
    MemoryRegionManager* manager;
    struct ibv_qp *queuePair;
    struct ibv_comp_channel *eventChannel;
    uint32_t messageDelayTime;
    struct ibv_cq *receiveCompletionQueue;
    uint32_t queueCapacity;
    char *metricURL;
    uint32_t numMetricAveraging;
    struct ibv_context *rdmaDeviceContext; // required for cleanup once transfers completed
    struct ibv_pd *protectionDomain; // required for cleanup once transfers completed
    struct ibv_cq *sendCompletionQueue; // required for cleanup once transfers completed
};


/**********************************************************************
 * Receives the RDMA work requests into the memory regions where the
 * parameters are provided in the struct receiveWorkRequestParams
 * When done, also deregisters memory regions, cleans up RDMA resources and frees ptr
 * Note if the work completions include immediate data then the receiver
 * checks that the immediate data values increment consecutively (or else
 * are reset using a smaller value than the previous immediate data value)
 **********************************************************************/
void *receiveWorkRequests(void *ptr)
{
    struct receiveWorkRequestParams *params = (struct receiveWorkRequestParams *)ptr;
    MemoryRegionManager* manager = params->manager;

    /* prepare scatter/gather array specifying buffers to read from or write into */
    /* note this implementation uses only one sgItem per work request but chains together */
    /* multiple work requests when numContiguousMessages > 1 */
    /* Alternatively, could instead use multiple sge, but then need to adjust queue pair specs */
    struct ibv_sge sgItems[manager->numMemoryRegions][manager->numContiguousMessages];
    memset(sgItems, 0, sizeof(struct ibv_sge[manager->numMemoryRegions][manager->numContiguousMessages]));
    int sgListLength = 1; /* only one sgItem per work request */

    /* prepare array of work requests across all memory regions for the receive queue */
    struct ibv_recv_wr workRequests[manager->numMemoryRegions][manager->numContiguousMessages];
    memset(workRequests, 0, sizeof(struct ibv_recv_wr[manager->numMemoryRegions][manager->numContiguousMessages]));
    for (uint32_t regionIndex=0; regionIndex<manager->numMemoryRegions; regionIndex++)
    {
        for (uint32_t i=0; i<manager->numContiguousMessages; i++)
        {
            /* prepare one sgItem to receive */
            sgItems[regionIndex][i].addr = (uint64_t)(manager->memoryRegions[regionIndex]->addr + i*manager->messageSize);
            sgItems[regionIndex][i].length = manager->messageSize;
            sgItems[regionIndex][i].lkey = manager->memoryRegions[regionIndex]->lkey;

            /* prepare one workRequest with this sgItem */
            workRequests[regionIndex][i].wr_id = (uint64_t)(regionIndex*manager->numContiguousMessages + i); /* gives memory region and location */
            workRequests[regionIndex][i].sg_list = &sgItems[regionIndex][i];
            workRequests[regionIndex][i].num_sge = sgListLength;
            if (i == manager->numContiguousMessages-1)
            {
                workRequests[regionIndex][i].next = NULL; /* note can chain multiple workRequest together for performance */
            }
            else
            {
                workRequests[regionIndex][i].next = &workRequests[regionIndex][i+1]; /* chain multiple workRequest together for performance */
            }
        }
    }

    /* setup for loop enqueueing blocks of work requests and polling for blocks of work completions */
    uint32_t minWorkRequestEnqueue = (uint32_t) ceil(params->queueCapacity * MIN_WORK_REQUEST_ENQUEUE);
    uint32_t maxWorkRequestDequeue = params->queueCapacity; /* try to completely drain queue of any completed work requests */
    struct ibv_recv_wr *badWorkRequest = NULL;
    struct ibv_wc *workCompletions = NULL;
    workCompletions = (struct ibv_wc*)malloc(sizeof(struct ibv_wc) * maxWorkRequestDequeue);
    if (workCompletions == NULL)
    {
        logger(LOG_ERR, "Receiver unable to allocate desired memory for work completions");
        return NULL;
    }
    setAllMemoryRegionsEnqueued(manager, false); 
    if (params->metricURL != NULL)
    {
        initialiseMetricReporter();
    }
    uint32_t currentQueueLoading = 0; /* no work requests initially in queue */
    uint64_t numWorkRequestsEnqueued = 0;
    uint32_t previousImmediateData = UINT32_MAX;
    if (previousImmediateData+(uint32_t)1 != (uint32_t)0)
    {
        logger(LOG_ERR, "Assertion that UINT32_MAX+1 == 0 has failed, so error checking immediate values");
    }
    uint64_t numWorkRequestsMissing = 0; /* determined by finding non-incrementing immediate data values */
    uint32_t regionIndex = 0;
    uint64_t numWorkRequestCompletions = 0;

    /* initialise receiver metrics */
    uint64_t metricMessagesTransferred = 0;
    clock_t metricStartClockTime = -1;
    struct timeval metricStartWallTime;
    uint64_t metricWorkRequestsMissing = 0;

    /* loop enqueueing blocks of work requests and polling for blocks of work completions */
    /* note there is one work completion signalled for each received message */
    while (numWorkRequestCompletions + numWorkRequestsMissing < manager->numTotalMessages)
    {
        /* post block of receive work requests to sufficiently full work request queue */
        while ((numWorkRequestsEnqueued + numWorkRequestsMissing < manager->numTotalMessages)
            && (currentQueueLoading < minWorkRequestEnqueue))
        {
            if (ibv_post_recv(params->queuePair, &workRequests[regionIndex][0], &badWorkRequest) != SUCCESS)
            {
                logger(LOG_ERR, "Unable to post receive request with error %d", errno);
            }
            else
            {
                logger(LOG_INFO, "Receiver has posted work requests for region %" PRIu32, regionIndex);
                /* check that the memory region isn't already populated with data */
                if (manager->isMonitoringRegions && getMemoryRegionPopulated(manager, regionIndex))
                {
                   logger(LOG_WARNING, "Memory region %" PRIu32 " has been enqueued for receiving"
                        " but already populated while enqueueing work request %" PRIu64,
                        regionIndex, numWorkRequestsEnqueued + numWorkRequestsMissing);
                }
                currentQueueLoading += manager->numContiguousMessages;
                numWorkRequestsEnqueued += manager->numContiguousMessages;
                setMemoryRegionEnqueued(manager, regionIndex, true);
                regionIndex++;
                if (regionIndex >= manager->numMemoryRegions)
                    regionIndex = 0;
                microSleep(params->messageDelayTime);
            }
        }

        /* poll for block of work completions */
        logger(LOG_INFO, "Receiver waiting for completion %" PRIu64 " of %" PRIu64,
            numWorkRequestCompletions + numWorkRequestsMissing, manager->numTotalMessages);
        if (waitForCompletionQueueEvent(params->eventChannel) != SUCCESS)
        {
            logger(LOG_ERR, "Receiver unable to wait for completion notification");
        }

        /* start the timer if first iteration */
        if (metricStartClockTime < 0)
        {
            metricStartClockTime = clock();
            gettimeofday(&metricStartWallTime, NULL);
        }

        /* poll the completion queue for work completions */
        memset(workCompletions, 0, sizeof(struct ibv_wc) * maxWorkRequestDequeue);
        int numCompletionsFound = 0;
        numCompletionsFound = ibv_poll_cq(params->receiveCompletionQueue, maxWorkRequestDequeue, workCompletions);
        if (numCompletionsFound < 0)
        {
            logger(LOG_WARNING, "Receiver failed to poll for work completions");
        }
        else
        {
            logger(LOG_INFO, "Receiver has polled %d work completions", numCompletionsFound);
            for (int wcIndex=0; wcIndex<numCompletionsFound; wcIndex++)
            {
                struct ibv_wc workCompletion = workCompletions[wcIndex];
                bool hasImmediateData = (workCompletion.wc_flags & IBV_WC_WITH_IMM) != 0;
                uint32_t immediateData = ntohl(workCompletion.imm_data);
                if (workCompletion.status != IBV_WC_SUCCESS)
                {
                    logger(LOG_ERR,
                        "Receiver work completion error with status:%d, id:%" PRIu64
                        ", imm_data:%" PRIu32 ", syndrome:0x%x",
                        workCompletion.status, workCompletion.wr_id, immediateData,
                        workCompletion.vendor_err);
                }
                else
                {
                    uint32_t transferredRegionIndex = (uint32_t)(workCompletion.wr_id / manager->numContiguousMessages);
                    uint32_t transferredContiguousIndex = (uint32_t)(workCompletion.wr_id % manager->numContiguousMessages);
                    setMemoryRegionEnqueued(manager, transferredRegionIndex, false); /* note this gets reset for each of the contiguous messages */
                    setMemoryRegionPopulated(manager, transferredRegionIndex, true); /* note typically now want to utilise the data and then unpopulate region */
                    setMessageTransferred(manager, transferredRegionIndex, transferredContiguousIndex, numWorkRequestCompletions+numWorkRequestsMissing);
                    logger(LOG_DEBUG,
                        "Receiver work completion %" PRIu64 " success status with work request id %" PRIu64
                        " and immediate data %" PRIu32, numWorkRequestCompletions + numWorkRequestsMissing,
                        workCompletion.wr_id, immediateData);
                }
                if (hasImmediateData)
                {
                    /* check immediate data value is incrementing contiguously */
                    if (immediateData == previousImmediateData)
                    {
                        /* identical work completion immediate data values received */
                        logger(LOG_WARNING, "Receiver work completion %" PRIu64 " immediate data %" PRIu32
                            " duplicated from previous messages", numWorkRequestCompletions + numWorkRequestsMissing,
                            immediateData);
                    }
                    else if (immediateData == previousImmediateData + (uint32_t)1) /* unsigned arithmetic with wraparound at UINT32_MAX */
                    {
                        /* immediateData has been incremented by one (possibly with wrap around) as expected */
                    }
                    else // if (immediateData - previousImmediateData > 1u) /* unsigned arithmetic with wraparound at UINT32_MAX */
                    {
                        /* there are missing work completions */
                        uint32_t numMissingFound = immediateData - (previousImmediateData+1u);
                        numWorkRequestsMissing += numMissingFound;
                        metricWorkRequestsMissing += numMissingFound;
                        logger(LOG_WARNING, "Receiver detected missing %" PRIu32
                            " message(s) while receiving work completion %" PRIu64,
                            numMissingFound, numWorkRequestCompletions + numWorkRequestsMissing);
                    }
                    previousImmediateData = immediateData;
                }
                currentQueueLoading--;
                numWorkRequestCompletions++;
                metricMessagesTransferred++;
            }
        }
        /* update receiver metrics, including partial metrics for last iteration of loop */
        if ((metricMessagesTransferred >= params->numMetricAveraging) || (numWorkRequestCompletions + numWorkRequestsMissing >= manager->numTotalMessages))
        {
            int queueUtilisation = (int)(100*currentQueueLoading/params->queueCapacity);
            logger(LOG_NOTICE, "Receiver queue currently at %d%% capacity", queueUtilisation);
            uint64_t durationClockMicroSec = (uint64_t)((clock() - metricStartClockTime) * 1000000L / CLOCKS_PER_SEC);
            struct timeval endWallTime;
            gettimeofday(&endWallTime, NULL);
            uint64_t durationWallMicroSec = (endWallTime.tv_sec - metricStartWallTime.tv_sec) * 1000000L
                + (endWallTime.tv_usec-metricStartWallTime.tv_usec);
            int cpuUtilisation = (int)round(100.0*durationClockMicroSec/durationWallMicroSec); /* only approx measure */
            uint64_t metricBytesTransferred = manager->messageSize * metricMessagesTransferred;
            double bandwidth = 8.0E-3 * metricBytesTransferred / durationWallMicroSec; /* Gbps */
            logger(LOG_NOTICE, "Receiver wall time duration since first message received is %" PRIu64 " milliseconds "
                "and CPU clock time duration is %" PRIu64 " milliseconds (%d%% utilisation)",
                durationWallMicroSec/1000, durationClockMicroSec/1000, cpuUtilisation);
            logger(LOG_NOTICE, "Receiver bandwidth is %.2f Gbps", bandwidth);
            int messageLoss = (int)round(100.0*metricWorkRequestsMissing/(metricMessagesTransferred+metricWorkRequestsMissing));
            logger(LOG_NOTICE, "Receiver detected %" PRIu64 " missing messages from %" PRIu64 " (%d%% missing)",
                metricWorkRequestsMissing, (metricMessagesTransferred+metricWorkRequestsMissing), messageLoss);
            uint64_t millisecondsSinceEpoch = endWallTime.tv_sec*1000L + endWallTime.tv_usec/1000;
            if (params->metricURL != NULL)
            {
                pushMetrics("RDMAreceiver", params->metricURL, queueUtilisation, cpuUtilisation, bandwidth,
                    metricMessagesTransferred, metricWorkRequestsMissing, millisecondsSinceEpoch, manager);
            }
            /* reset receiver metrics */
            metricMessagesTransferred = 0;
            metricStartClockTime = clock();
            gettimeofday(&metricStartWallTime, NULL);
            metricWorkRequestsMissing = 0;
        }
        if ((currentQueueLoading == 0) && (numWorkRequestCompletions + numWorkRequestsMissing < manager->numTotalMessages))
            logger(LOG_WARNING, "Receiver queue currently empty, message loss possible");
    }

    if (params->metricURL != NULL)
    {
        cleanupMetricReporter();
    }
    free(workCompletions);

    /* clean up allocated resources */
    deregisterMemoryRegions(manager);
    cleanupRDMAResources(params->rdmaDeviceContext, params->eventChannel, params->protectionDomain,
        params->receiveCompletionQueue, params->sendCompletionQueue, params->queuePair);
    free(params); // free malloc in rdmaTransferMemoryBlocks
    return NULL;
}


/**********************************************************************
 * Deregister allocated memory regions and free memoryRegions
 **********************************************************************/
void deregisterMemoryRegions(MemoryRegionManager* manager)
{
    if (manager->memoryRegions == NULL)
    {
        logger(LOG_WARNING, "Attempt to deregister null memory regions of memory region manager");
        return;
    }
    /* deregister each memory region and free the memory region array */
    for (uint32_t regionIndex=0; regionIndex<manager->numMemoryRegions; regionIndex++)
    {
        if (ibv_dereg_mr(manager->memoryRegions[regionIndex]) != SUCCESS)
            logger(LOG_ERR, "Unable to deregister memory region %" PRIu32, regionIndex);
    }
    free(manager->memoryRegions);
    manager->memoryRegions = NULL;
}


/**********************************************************************
 * Cleans up allocated RDMA resources
 **********************************************************************/
void cleanupRDMAResources(struct ibv_context *rdmaDeviceContext,
    struct ibv_comp_channel *eventChannel,
    struct ibv_pd *protectionDomain,
    struct ibv_cq *receiveCompletionQueue,
    struct ibv_cq *sendCompletionQueue,
    struct ibv_qp *queuePair)
{
    logger(LOG_DEBUG, "Cleaning up allocated RDMA resources");
    if (ibv_destroy_qp(queuePair) != SUCCESS)
        logger(LOG_ERR, "Unable to destroy queue pair");
    if (sendCompletionQueue != receiveCompletionQueue)
    {
        if (ibv_destroy_cq(sendCompletionQueue) != SUCCESS)
            logger(LOG_ERR, "Unable to destroy send completion queue");
    }
    if (ibv_destroy_cq(receiveCompletionQueue) != SUCCESS)
        logger(LOG_ERR, "Unable to destroy receive completion queue");
    if (ibv_dealloc_pd(protectionDomain) != SUCCESS)
        logger(LOG_ERR, "Unable to deallocate protection domain");
    if (ibv_destroy_comp_channel(eventChannel) != SUCCESS)
        logger(LOG_ERR, "Unable to destroy event completion channel");
    if (ibv_close_device(rdmaDeviceContext) != SUCCESS)
        logger(LOG_ERR, "Unable to close RDMA device");
}


/**********************************************************************
 * Convenience function to prepare the RDMA device and then send or receive RDMA
 * using allocated memory blocks and the default detected RDMA device, RDMA port, GID
 * Note if identifierExchangeFunction==NULL then default to using stdio for exchanging RDMA identifiers
 **********************************************************************/
int rdmaTransferMemoryBlocksWithDefaultDevice(enum runMode mode, MemoryRegionManager *manager, uint32_t messageDelayTime,
    enum exchangeResult (*identifierExchangeFunction)(bool isSendMode,
        uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
        uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr))
{
    return rdmaTransferMemoryBlocks(mode, manager, messageDelayTime, identifierExchangeFunction,
        NULL, 1, -1, NULL, 0);
}

/**********************************************************************
 * Convenience function to prepare the RDMA device and then send or receive RDMA
 * using allocated memory blocks
 * Note if identifierExchangeFunction==NULL then default to using stdio for exchanging RDMA identifiers
 * Note if rdmaDeviceName==NULL then no preferred rdma device name to choose
 * Note if gidIndex = -1 then no preferred gid index to use
 **********************************************************************/
int rdmaTransferMemoryBlocks(enum runMode mode, MemoryRegionManager *manager, uint32_t messageDelayTime,
    enum exchangeResult (*identifierExchangeFunction)(bool isSendMode,
        uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
        uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr),
    char *rdmaDeviceName, uint8_t rdmaPort, int gidIndex, char *metricURL, uint32_t numMetricAveraging)
{
    if (numMetricAveraging == 0)
        numMetricAveraging = manager->numMemoryRegions * manager->numContiguousMessages;

    logger(LOG_INFO, "Receive Visibilities starting");
    if (mode == SEND_MODE)
        logger(LOG_INFO, "Running as sender");
    else
        logger(LOG_INFO, "Running as receiver");

    /* initialise libibverb data structures so have fork() protection */
    /* note this has a performance hit, and is optional */
    // enableForkProtection();

    /* allocate all the RDMA resources */
    uint32_t queueCapacity = manager->numContiguousMessages * manager->numMemoryRegions ; /* minimum capacity for completion queues */
    uint32_t maxInlineDataSize = 0; /* NOTE put back at 236 once testing completed */;
    struct ibv_context *rdmaDeviceContext;
    struct ibv_comp_channel *eventChannel;
    struct ibv_pd *protectionDomain;
    struct ibv_cq *receiveCompletionQueue;
    struct ibv_cq *sendCompletionQueue;
    struct ibv_qp *queuePair;
    if (allocateRDMAResources(rdmaDeviceName, rdmaPort, queueCapacity, maxInlineDataSize,
        &rdmaDeviceContext, &eventChannel, &protectionDomain, &receiveCompletionQueue,
        &sendCompletionQueue, &queuePair) != SUCCESS)
    {
        logger(LOG_CRIT, "Unable to allocate the RDMA resources");
        return FAILURE;
    }
    else
        logger(LOG_DEBUG, "RDMA resources allocated");

    /* set up the RDMA connection */
    uint16_t localIdentifier;
    union ibv_gid gidAddress;
    enum ibv_mtu mtu;
    if (setupRDMAConnection(rdmaDeviceContext, rdmaPort, &localIdentifier,
        &gidAddress, &gidIndex, &mtu) != SUCCESS)
    {
        logger(LOG_CRIT, "Unable to set up the RDMA connection");
        return FAILURE;
    }
    else
        logger(LOG_DEBUG, "RDMA connection set up");

    logger(LOG_INFO, "RDMA connection initialised on local %s", mode ? "sender" : "receiver");

    /* initialise the random number generator to generate a 24-bit psn */
    srand(getpid() * time(NULL));
    uint32_t packetSequenceNumber = (uint32_t) (rand() & 0x00FFFFFF);

    /* exchange the necessary information with the remote RDMA peer */
    uint32_t queuePairNumber = queuePair->qp_num;
    uint32_t remotePSN;
    uint32_t remoteQPN;
    union ibv_gid remoteGID;
    uint16_t remoteLID;
    if (identifierExchangeFunction == NULL)
    {
        if (exchangeViaStdIO(mode==SEND_MODE,
            packetSequenceNumber, queuePairNumber, gidAddress, localIdentifier,
            &remotePSN, &remoteQPN, &remoteGID, &remoteLID) != EXCH_SUCCESS)
        {
            logger(LOG_CRIT, "Unable to exchange identifiers via standard IO");
            return FAILURE;
        }
    }
    else
    {
        if (identifierExchangeFunction(mode==SEND_MODE,
            packetSequenceNumber, queuePairNumber, gidAddress, localIdentifier,
            &remotePSN, &remoteQPN, &remoteGID, &remoteLID) != EXCH_SUCCESS)
        {
            logger(LOG_CRIT, "Unable to exchange identifiers via provided exchange function");
            return FAILURE;
        }

    }

    /* modify the queue pair to be ready to receive and possibly send */
    if (modifyQueuePairReady(queuePair, rdmaPort, gidIndex, mode, packetSequenceNumber,
        remotePSN, remoteQPN, remoteGID, remoteLID, mtu) != SUCCESS)
    {
        logger(LOG_CRIT, "Unable to modify queue pair to be ready");
        return FAILURE;
    }
    else
        logger(LOG_DEBUG, "Queue pair modified to be ready");

    /* register memory blocks as memory regions for buffer */
    if (registerMemoryRegions(protectionDomain, manager) != SUCCESS)
    {
        logger(LOG_CRIT, "Unable to allocate memory regions");
        return FAILURE;
    }

    /* perform the rdma data transfer */
    if (ibv_req_notify_cq(receiveCompletionQueue, 0) != SUCCESS)
    {
        logger(LOG_ERR, "Unable to request receive completion queue notifications");
        if (mode == RECV_MODE)
            return FAILURE;
    }
    if (ibv_req_notify_cq(sendCompletionQueue, 0) != SUCCESS)
    {
        logger(LOG_WARNING, "Unable to request send completion queue notifications");
    }
    if (mode == SEND_MODE)
    {
        struct sendWorkRequestParams *params = NULL;
        params = (struct sendWorkRequestParams*)malloc(sizeof(struct sendWorkRequestParams)); // freed in sendWorkRequests
        params->manager = manager;
        params->queuePair = queuePair;
        params->eventChannel = eventChannel;
        params->messageDelayTime = messageDelayTime;
        params->maxInlineDataSize = maxInlineDataSize;
        params->sendCompletionQueue = sendCompletionQueue;
        params->queueCapacity = queueCapacity;
        params->metricURL = metricURL;
        params->numMetricAveraging = numMetricAveraging;
        params->rdmaDeviceContext = rdmaDeviceContext;
        params->protectionDomain = protectionDomain;
        params->receiveCompletionQueue = receiveCompletionQueue;
        if (pthread_create(&(manager->enqueueWorkRequestsThread), NULL, sendWorkRequests, (void*)params) != 0)
            logger(LOG_ERR, "Unable to create thread for sending work requests");
    }
    else /* mode == RECV_MODE */
    {
        struct receiveWorkRequestParams *params = NULL;
        params = (struct receiveWorkRequestParams*)malloc(sizeof(struct receiveWorkRequestParams)); // freed in receiveWorkRequests
        params->manager = manager;
        params->queuePair = queuePair;
        params->eventChannel = eventChannel;
        params->messageDelayTime = messageDelayTime;
        params->receiveCompletionQueue = receiveCompletionQueue;
        params->queueCapacity = queueCapacity;
        params->metricURL = metricURL;
        params->numMetricAveraging = numMetricAveraging;
        params->rdmaDeviceContext = rdmaDeviceContext;
        params->protectionDomain = protectionDomain;
        params->sendCompletionQueue = sendCompletionQueue;
        if (pthread_create(&(manager->enqueueWorkRequestsThread), NULL, receiveWorkRequests, (void*)params) != 0)
            logger(LOG_ERR, "Unable to create thread for receiving work requests");
    }

    logger(LOG_INFO, "Receive Visibilities ending");
    return SUCCESS;
}


/**********************************************************************
 * Convenience function to wait until rdmaTransferMemoryBlocks has completed
 **********************************************************************/
int waitForRdmaTransferCompletion(MemoryRegionManager *manager)
{
    if (pthread_join(manager->enqueueWorkRequestsThread, NULL) != 0)
    {
        logger(LOG_WARNING, "Unable to have current thread join the manager working thread");
        return FAILURE;
    }
    return SUCCESS;
}

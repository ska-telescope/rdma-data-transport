// Copyright 2019 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * RDMAmemorymanager.c
 * Andrew Ensor
 * C API for tracking memory region usage when using RDMA
 *
*/

#include "RDMAmemorymanager.h"

/**********************************************************************
 * Convenience function that allocates page aligned memory blocks suitable
 * for passing to a MemoryRegionManager
 * Should free the memory blocks when done via freeMemoryBlocks
 * Note that contents of numMemoryBlocks will be modified if not all
 * the requested memory blocks could be allocated
 **********************************************************************/
void** allocateMemoryBlocks(uint64_t memoryBlockSize, uint32_t *numMemoryBlocks)
{
    /* allocate memory region for buffer */
    uint64_t pageSize = sysconf(_SC_PAGESIZE);
    if (pageSize == 0)
        pageSize = 4096; /* default page size which should be a power of two */

    void **memoryBlocks = NULL;
    memoryBlocks = (void**)malloc(sizeof(void *) * (*numMemoryBlocks));
    if (memoryBlocks == NULL)
    {
        logger(LOG_ERR, "Unable to create memory block array");
        *numMemoryBlocks = 0;
        return memoryBlocks;
    }
    uint32_t numBlocksAllocated = 0;
    void *memoryBlock = NULL;
    do
    {
        /* allocate memory for the next memory block */
        memoryBlock = memalign(pageSize, memoryBlockSize);
        if (memoryBlock == NULL)
        {
            logger(LOG_ERR, "Unable to create memory block with %llu bytes", memoryBlockSize);
        }
        else
        {
            memoryBlocks[numBlocksAllocated] = memoryBlock;
            numBlocksAllocated++;
        }
    }
    while ((numBlocksAllocated < *numMemoryBlocks) && (memoryBlock != NULL));

    if (numBlocksAllocated < *numMemoryBlocks)
    {
        logger(LOG_WARNING, "Only allocated %" PRIu32 " of the requested %" PRIu32 " memory blocks",
            numBlocksAllocated, *numMemoryBlocks);
    }
    else
    {
        logger(LOG_INFO, "Successfully allocated all %" PRIu32 " memory blocks", *numMemoryBlocks);
    }
    *numMemoryBlocks = numBlocksAllocated;
    return memoryBlocks;
}


/**********************************************************************
 * Creates a MemoryRegionManager struct to report on status of a
 * collection of memory regions used for RDMA (sending or receiving)
 **********************************************************************/
MemoryRegionManager* createMemoryRegionManager(void **memoryBlocks,
    uint32_t messageSize, uint32_t numMemoryBlocks, uint32_t numContiguousMessages,
    uint64_t numTotalMessages, bool isMonitoringRegions)
{
    MemoryRegionManager *manager = NULL;
    manager = (MemoryRegionManager*)malloc(sizeof(MemoryRegionManager));
    manager->memoryBlocks = memoryBlocks;
    if (memoryBlocks == NULL)
    {
        logger(LOG_WARNING, "MemoryRegionManager has been created with no memory blocks specified");
    }
    manager->messageSize = messageSize;
    manager->numMemoryRegions = numMemoryBlocks;
    manager->numContiguousMessages = numContiguousMessages;
    manager->numTotalMessages = numTotalMessages;
    manager->memoryBlockSize = messageSize * numContiguousMessages;
    manager->memoryRegions = NULL; /* set when the memory blocks get registered as RDMA memory regions */
    manager->isMonitoringRegions = isMonitoringRegions;
    manager->populated = (bool*)calloc(numMemoryBlocks, sizeof(bool)); /* initially all false */
    manager->enqueued = (bool*)calloc(numMemoryBlocks, sizeof(bool)); /* initially all false */
    manager->messageTransferred = (uint64_t*)malloc(numMemoryBlocks*numContiguousMessages*sizeof(uint64_t));
    for (uint i=0; i<numMemoryBlocks*numContiguousMessages; i++)
    {
        manager->messageTransferred[i] = NO_MESSAGE_ORDINAL; /* initially no messages transferred in any of the contiguous memory locations */
    }
    int mutexInit = pthread_mutex_init(&manager->mutex, NULL);
    if (mutexInit != 0)
    {
        logger(LOG_ERR, "Mutex initialization error %d while creating memory manager", mutexInit);
    }
    logger(LOG_INFO, "Created memory manager");
    return manager;
}


/**********************************************************************
 * Convenience function that reads all the data into the MemoryRegionManager
 * from dataFileName.0, dataFileName.1, ...
 * Note first int in each file is assumed to be the number of 8 byte values
 * in the file which gets discarded
 * Returns true if the files were successfully read into the memory blocks
 **********************************************************************/
bool readFilesIntoMemoryBlocks(MemoryRegionManager *manager, const char *dataFileName)
{
    bool isSuccessfulRead = true;
    for (uint32_t blockIndex=0; blockIndex<manager->numMemoryRegions; blockIndex++)
    {
        int fileSuffixLength = 2 + snprintf(NULL, 0, "%d", blockIndex);
        char fileNameWithSuffix[strlen(dataFileName)+fileSuffixLength];
        memset(fileNameWithSuffix, '\0', sizeof(fileNameWithSuffix));
        strcpy(fileNameWithSuffix, dataFileName);
        strcat(fileNameWithSuffix, ".");
        char suffix[fileSuffixLength-1];
        memset(suffix, '\0', sizeof(suffix));
        sprintf(suffix, "%d", blockIndex);
        strcat(fileNameWithSuffix, suffix);
        if (access(fileNameWithSuffix, F_OK|R_OK) != 0)
        {
            logger(LOG_ERR, "Unable to read from data file %s", fileNameWithSuffix);
            isSuccessfulRead = false;
        }
        FILE *filePointer;
        filePointer = fopen(fileNameWithSuffix, "rb");
        int numBytesInFile;
        size_t numBytesRead;
        numBytesRead = fread(&numBytesInFile, sizeof(int), 1, filePointer);
        numBytesInFile *= 8;
        numBytesRead = fread(manager->memoryBlocks[blockIndex], 1, manager->memoryBlockSize, filePointer);
        if (numBytesRead < manager->memoryBlockSize)
        {
            logger(LOG_WARNING, "Only read %d of %d bytes from data file %s which should contain %d bytes",
                numBytesRead, manager->memoryBlockSize, fileNameWithSuffix, numBytesInFile); 
        }
        logger(LOG_INFO, "Have read %d bytes from data file %s", numBytesRead, fileNameWithSuffix);
        fclose(filePointer);
    }
    return isSuccessfulRead;
}


/**********************************************************************
 * Convenience function that writes all the data from the MemoryRegionManager
 * to dataFileName.0, dataFileName.1, ...
 * Note first int in each file is the number of 8 byte values in memory block
 * Returns true if the files were successfully written from the memory blocks
 **********************************************************************/
bool writeFilesFromMemoryBlocks(MemoryRegionManager *manager, const char *dataFileName)
{
    bool isSuccessfulWrite = true;
    for (uint32_t blockIndex=0; blockIndex<manager->numMemoryRegions; blockIndex++)
    {
        int fileSuffixLength = 2 + snprintf(NULL, 0, "%d", blockIndex);
        char fileNameWithSuffix[strlen(dataFileName)+fileSuffixLength];
        memset(fileNameWithSuffix, '\0', sizeof(fileNameWithSuffix));
        strcpy(fileNameWithSuffix, dataFileName);
        strcat(fileNameWithSuffix, ".");
        char suffix[fileSuffixLength-1];
        memset(suffix, '\0', sizeof(suffix));
        sprintf(suffix, "%d", blockIndex);
        strcat(fileNameWithSuffix, suffix);
        if ((access(fileNameWithSuffix, 0) == 0) && (access(fileNameWithSuffix, W_OK) != 0))
        {
            logger(LOG_ERR, "Unable to write to data file %s", fileNameWithSuffix);
            isSuccessfulWrite = false;
        }
        FILE *filePointer;
        filePointer = fopen(fileNameWithSuffix, "wb");
        int num8BytesInFile = (int)ceil(manager->memoryBlockSize/8.0);
        size_t numBytesWritten;
        numBytesWritten = fwrite(&num8BytesInFile, sizeof(int), 1, filePointer);
        numBytesWritten = fwrite(manager->memoryBlocks[blockIndex], 1, manager->memoryBlockSize, filePointer);
        if (numBytesWritten < manager->memoryBlockSize)
        {
            logger(LOG_WARNING, "Only written %d of %d bytes to data file %s",
                numBytesWritten, manager->memoryBlockSize, fileNameWithSuffix); 
        }
        logger(LOG_INFO, "Have written %d bytes to data file %s", numBytesWritten, fileNameWithSuffix);
        fclose(filePointer);
    }
    return isSuccessfulWrite;
}


/**********************************************************************
 * Convenience function that displays the specified number of bytes
 * from the start and end of each memory block in the MemoryRegionManager
 **********************************************************************/
void displayMemoryBlocks(MemoryRegionManager *manager, uint32_t startBytes, uint32_t endBytes)
{
    printf("######################################################################\n");
    printf("Memory contents:\n");
    for (uint32_t blockIndex=0; blockIndex<manager->numMemoryRegions; blockIndex++)
    {
        printf("  Memory block %04d: ", blockIndex);
        if (manager->memoryBlocks[blockIndex] == NULL)
            printf("NULL memory block\n");
        else
        {
            unsigned char *bytePtr = (unsigned char *)(manager->memoryBlocks[blockIndex]);
            if (startBytes > manager->memoryBlockSize)
                startBytes = manager->memoryBlockSize;
            for (uint64_t i=0; i<startBytes; i++)
            {
                printf(" %02hhx", bytePtr[i]);
            }
            if (endBytes > (manager->memoryBlockSize - startBytes))
                endBytes = manager->memoryBlockSize - startBytes;
            if ((startBytes + endBytes) < manager->memoryBlockSize)
            {
                printf(" ...");
            }
            for (uint64_t i=manager->memoryBlockSize-endBytes; i<manager->memoryBlockSize; i++)
            {
                printf(" %02hhx", bytePtr[i]);
            }
            printf("\n");
        }
    }
    printf("######################################################################\n");
}


/**********************************************************************
 * Sets the populated status of all memory regions
 **********************************************************************/
void setAllMemoryRegionsPopulated(MemoryRegionManager *manager, bool status)
{
    int mutexLock = pthread_mutex_lock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex lock error %d while setting all memory regions populated", mutexLock);
    }
    for (uint32_t regionIndex=0; regionIndex<manager->numMemoryRegions; regionIndex++)
    {
        manager->populated[regionIndex] = status;
    }
    mutexLock = pthread_mutex_unlock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex unlock error %d while setting all memory regions populated", mutexLock);
    }
}

/**********************************************************************
 * Gets the populated status of all specified memory regions
 * Note this function presumes populatedRegions is sufficiently large to
 * hold results
 **********************************************************************/
void getAllMemoryRegionsPopulated(MemoryRegionManager *manager, bool *populatedRegions)
{
    int mutexLock = pthread_mutex_lock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex lock error %d while getting all memory regions populated", mutexLock);
    }
    memcpy(populatedRegions, manager->populated, manager->numMemoryRegions*sizeof(bool));
    mutexLock = pthread_mutex_unlock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex unlock error %d while getting all memory regions populated", mutexLock);
    }
}

/**********************************************************************
 * Sets the populated status of a specified memory region
 **********************************************************************/
void setMemoryRegionPopulated(MemoryRegionManager *manager, uint32_t regionIndex, bool status)
{
    int mutexLock = pthread_mutex_lock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex lock error %d while setting a memory region populated", mutexLock);
    }
    manager->populated[regionIndex] = status;
    mutexLock = pthread_mutex_unlock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex unlock error %d while setting a memory region populated", mutexLock);
    }
}

/**********************************************************************
 * Gets the populated status of a specified memory region
 **********************************************************************/
bool getMemoryRegionPopulated(MemoryRegionManager *manager, uint32_t regionIndex)
{
    int mutexLock = pthread_mutex_lock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex lock error %d while getting a memory regions populated", mutexLock);
    }
    bool status = manager->populated[regionIndex];
    mutexLock = pthread_mutex_unlock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex unlock error %d while getting a memory regions populated", mutexLock);
    }
    return status;
}

/**********************************************************************
 * Sets the enqueued status of all memory regions
 **********************************************************************/
void setAllMemoryRegionsEnqueued(MemoryRegionManager *manager, bool status)
{
    int mutexLock = pthread_mutex_lock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex lock error %d while setting all memory regions enqueued", mutexLock);
    }
    for (uint32_t regionIndex=0; regionIndex<manager->numMemoryRegions; regionIndex++)
    {
        manager->enqueued[regionIndex] = status;
    }
    mutexLock = pthread_mutex_unlock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex unlock error %d while setting all memory regions enqueued", mutexLock);
    }
}

/**********************************************************************
 * Gets the enqueued status of all specified memory regions
 * Note this function presumes enqueuedRegions is sufficiently large to
 * hold results
 **********************************************************************/
void getAllMemoryRegionsEnqueued(MemoryRegionManager *manager, bool *enqueuedRegions)
{
    int mutexLock = pthread_mutex_lock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex lock error %d while getting all memory regions enqueued", mutexLock);
    }
    memcpy(enqueuedRegions, manager->enqueued, manager->numMemoryRegions*sizeof(bool));
    mutexLock = pthread_mutex_unlock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex unlock error %d while getting all memory regions enqueued", mutexLock);
    }
}

/**********************************************************************
 * Sets the enqueued status of a specified memory region
 **********************************************************************/
void setMemoryRegionEnqueued(MemoryRegionManager *manager, uint32_t regionIndex, bool status)
{
    int mutexLock = pthread_mutex_lock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex lock error %d while setting all memory regions enqueued", mutexLock);
    }
    manager->enqueued[regionIndex] = status;
    mutexLock = pthread_mutex_unlock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex unlock error %d while setting all memory regions enqueued", mutexLock);
    }
}

/**********************************************************************
 * Gets the enqueued status of a specified memory region
 **********************************************************************/
bool getMemoryRegionEnqueued(MemoryRegionManager *manager, uint32_t regionIndex)
{
    int mutexLock = pthread_mutex_lock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex lock error %d while getting a memory region enqueued", mutexLock);
    }
    bool status = manager->enqueued[regionIndex];
    mutexLock = pthread_mutex_unlock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex unlock error %d while getting a memory region enqueued", mutexLock);
    }
    return status;
}

/**********************************************************************
 * Sets the transferred message ordinal for specified memory location
 **********************************************************************/
void setMessageTransferred(MemoryRegionManager *manager, uint32_t regionIndex, uint32_t contiguousIndex, uint64_t messageOrdinal)
{
    int mutexLock = pthread_mutex_lock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex lock error %d while setting message held", mutexLock);
    }
    manager->messageTransferred[regionIndex*manager->numContiguousMessages+contiguousIndex] = messageOrdinal;
    mutexLock = pthread_mutex_unlock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex unlock error %d while setting message held", mutexLock);
    }
}

/**********************************************************************
 * Gets the transferred message ordinal for specified memory location
 **********************************************************************/
uint64_t getMessageTransferred(MemoryRegionManager *manager, uint32_t regionIndex, uint32_t contiguousIndex)
{
    int mutexLock = pthread_mutex_lock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex lock error %d while getting an ordinal of a message held", mutexLock);
    }
    uint64_t messageOrdinal = manager->messageTransferred[regionIndex*manager->numContiguousMessages+contiguousIndex];
    mutexLock = pthread_mutex_unlock(&manager->mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex unlock error %d while getting an ordinal of a message held", mutexLock);
    }
    return messageOrdinal;
}

/**********************************************************************
 * Destroys a MemoryRegionManager struct that was used for reporting
 **********************************************************************/
void destroyMemoryRegionManager(MemoryRegionManager *manager)
{
    logger(LOG_INFO, "Destroying memory manager");
    pthread_mutex_t mutex = manager->mutex;
    int mutexLock = pthread_mutex_lock(&mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex lock error %d while destroying memory manager", mutexLock);
    }
    free(manager->populated);
    manager->populated = NULL;
    free(manager->enqueued);
    manager->enqueued = NULL;
    free(manager->messageTransferred);
    manager->messageTransferred = NULL;
    free(manager);
    manager = NULL;
    mutexLock = pthread_mutex_unlock(&mutex);
    if (mutexLock != 0)
    {
        logger(LOG_ERR, "Mutex unlock error %d while destroying memory manager", mutexLock);
    }
    int mutexDestroy = pthread_mutex_destroy(&mutex);
    if (mutexDestroy != 0)
    {
        logger(LOG_ERR, "Mutex destroy error %d while destroying memory manager", mutexDestroy);
    }
}


/**********************************************************************
 * Convenience function that frees allocated memory blocks created by allocateMemoryBlocks
 **********************************************************************/
void freeMemoryBlocks(void **memoryBlocks, uint32_t numMemoryBlocks)
{
    /* free each memory block and the memory region array */
    for (uint32_t blockIndex=0; blockIndex<numMemoryBlocks; blockIndex++)
    {
        free(memoryBlocks[blockIndex]);
    }
    free(memoryBlocks);
}

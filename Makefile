
# Args for Base Images
BASE_BUILD_IMAGE ?= ubuntu:18.04
RUNTIME_IMAGE ?= ubuntu:18.04

# Args for K8s and charts
NAME := rdma-data-transport
KUBE_NAMESPACE ?= "default"
KUBECTL_VERSION ?= latest
HELM_VERSION ?= v3.1.2
# See : https://quay.io/repository/helmpack/chart-testing?tab=tags
CT_TAG ?= v3.0.0-beta.2
HELM_CHART = $(NAME)
HELM_RELEASE ?= test-receive

# Args for image
CI_REGISTRY ?= docker.io
CI_REPOSITORY ?= piersharding
TAG ?= $(shell git rev-parse --verify --short=8 HEAD)
IMAGE ?= $(CI_REPOSITORY)/$(NAME):$(TAG)

# define your personal overides for above variables in here
-include PrivateRules.mak

.PHONY: vars help compile test clean build run up down k8s show lint deploy delete logs describe namespace default all clean
.DEFAULT_GOAL := help

TARGET = receive
LIBS = -lm -lrdmacm -libverbs -lcurl -lpthread
CC = gcc
CFLAGS = -fPIC -g -Wall -Wextra -D_GNU_SOURCE -O3

default: $(TARGET)

all: compile  ## Build the $(TARGET)

debug: CFLAGS += -g3 -DDEBUG
debug: compile

OBJECTS = $(patsubst %.c, %.o, $(wildcard *.c))
HEADERS = $(wildcard *.h)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(OBJECTS)
	$(CC) $(OBJECTS) $(LIBS) -o $@

RDMAOBJECTS = $(patsubst %.c, %.o, $(wildcard RDMA*.c))
GENERATEDLIB = librdmaapi

clean:
	-rm -f $(OBJECTS)
	-rm -f $(TARGET)
	-rm -f $(GENERATEDLIB).so
	-rm -f $(GENERATEDLIB).a

# Set dir of Makefile to a variable to use later
MAKEPATH := $(abspath $(lastword $(MAKEFILE_LIST)))
BASEDIR := $(patsubst %/,%,$(dir $(MAKEPATH)))

vars: ## make variables
	@echo "BASEDIR: $(BASEDIR)"
	@echo "OBJECTS: $(OBJECTS)"
	@echo "TARGET: $(TARGET)"
	@echo "RDMAOBJECTS: $(RDMAOBJECTS)"
	@echo "GENERATEDLIB: $(GENERATEDLIB)"
	@echo "CI_REGISTRY: $(CI_REGISTRY)"
	@echo "CI_REPOSITORY: $(CI_REPOSITORY)"
	@echo "TAG: $(TAG)"
	@echo "IMAGE: $(IMAGE)"

compile: vars default ## compile app

build: compile ## alias for compile

libs: $(RDMAOBJECTS)
	gcc -shared -o $(GENERATEDLIB).so $(RDMAOBJECTS)
	ar -rcs $(GENERATEDLIB).a $(RDMAOBJECTS)

run: ## run $(TARGET)
	docker run --rm  --privileged -ti $(IMAGE) bash

image: ## build Docker image
	docker build \
	  --build-arg BASE_BUILD_IMAGE=$(BASE_BUILD_IMAGE) \
		--build-arg RUNTIME_IMAGE=$(RUNTIME_IMAGE) \
	  -t $(NAME):latest -f Dockerfile .

tag: image ## tag image with tag and latest
	docker tag $(NAME):latest $(IMAGE)
	docker tag $(NAME):latest $(CI_REPOSITORY)/$(NAME):latest

push: tag ## push image tag and latest
	docker push $(IMAGE)
	docker push $(CI_REPOSITORY)/$(NAME):latest

k8s: ## Which kubernetes are we connected to
	@echo "Kubernetes cluster-info:"
	@kubectl cluster-info
	@echo ""
	@echo "kubectl version:"
	@kubectl version
	@echo ""
	@echo "Helm version:"
	@helm version --client
	@echo ""
	@echo "Helm plugins:"
	@helm plugin list

logs: ## show receiver POD logs
	@for i in `kubectl -n $(KUBE_NAMESPACE) get pods -l app.kubernetes.io/instance=$(HELM_RELEASE) -o=name`; \
	do \
	echo "---------------------------------------------------"; \
	echo "Logs for $${i}"; \
	echo kubectl -n $(KUBE_NAMESPACE) logs $${i}; \
	echo kubectl -n $(KUBE_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"; \
	echo "---------------------------------------------------"; \
	for j in `kubectl -n $(KUBE_NAMESPACE) get $${i} -o jsonpath="{.spec.initContainers[*].name}"`; do \
	RES=`kubectl -n $(KUBE_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
	echo "initContainer: $${j}"; echo "$${RES}"; \
	echo "---------------------------------------------------";\
	done; \
	echo "Main Pod logs for $${i}"; \
	echo "---------------------------------------------------"; \
	for j in `kubectl -n $(KUBE_NAMESPACE) get $${i} -o jsonpath="{.spec.containers[*].name}"`; do \
	RES=`kubectl -n $(KUBE_NAMESPACE) logs $${i} -c $${j} 2>/dev/null`; \
	echo "Container: $${j}"; echo "$${RES}"; \
	echo "---------------------------------------------------";\
	done; \
	echo "---------------------------------------------------"; \
	echo ""; echo ""; echo ""; \
	done

redeploy: delete deploy  ## redeploy rdma-data-transport

namespace: ## create the kubernetes namespace
	kubectl describe namespace $(KUBE_NAMESPACE) || kubectl create namespace $(KUBE_NAMESPACE)

delete_namespace: ## delete the kubernetes namespace
	@if [ "default" == "$(KUBE_NAMESPACE)" ] || [ "kube-system" == "$(KUBE_NAMESPACE)" ]; then \
	echo "You cannot delete Namespace: $(KUBE_NAMESPACE)"; \
	exit 1; \
	else \
	kubectl describe namespace $(KUBE_NAMESPACE) && kubectl delete namespace $(KUBE_NAMESPACE); \
	fi

deploy: install

install: namespace  ## install the helm chart
	@helm install $(HELM_RELEASE) charts/$(HELM_CHART)/ \
				--wait \
				--namespace $(KUBE_NAMESPACE) \
				--set testDataPath=$(BASEDIR)/ \
				 --set receive.image=$(IMAGE)

helm_delete: ## delete the helm chart release
	@helm delete $(HELM_RELEASE)

show: ## show the helm chart
	helm template $(HELM_RELEASE) charts/$(HELM_CHART)/ \
				 --namespace $(KUBE_NAMESPACE) \
				 --set testDataPath=$(BASEDIR)/ \
				 --set receive.image=$(IMAGE)

lint: vars ## lint check the helm chart
	# Chart testing: https://github.com/helm/chart-testing
	@helm lint charts/$(HELM_CHART)/ \
				 --namespace $(KUBE_NAMESPACE) \
				 --set testDataPath=$(BASEDIR)/ \
				 --set receive.image=$(IMAGE)
	@docker run --rm -ti \
	  --volume $(BASEDIR):/app \
	  quay.io/helmpack/chart-testing:$(CT_TAG) \
	  sh -c 'cd /app; ct lint --config ci/ct.yaml --all'

delete: helm_delete ## delete the helm chart release

describe: ## describe Pods executed from Helm chart
	@for i in `kubectl -n $(KUBE_NAMESPACE) get pods -l app.kubernetes.io/instance=$(HELM_RELEASE) -o=name`; \
	do echo "---------------------------------------------------"; \
	echo "Describe for $${i}"; \
	echo kubectl -n $(KUBE_NAMESPACE) describe $${i}; \
	echo "---------------------------------------------------"; \
	kubectl -n $(KUBE_NAMESPACE) describe $${i}; \
	echo "---------------------------------------------------"; \
	echo ""; echo ""; echo ""; \
	done

helm_dependencies: ## Utility target to install Helm dependencies
	@which helm ; rc=$$?; \
	if [ $$rc != 0 ]; then \
	curl "https://get.helm.sh/helm-$(HELM_VERSION)-linux-amd64.tar.gz" | tar zx; \
	sudo mv -f linux-amd64/helm /usr/local/bin/; \
	rm -rf linux-amd64; \
	else \
	helm version | grep $(HELM_VERSION); rc=$$?; \
	if  [ $$rc != 0 ]; then \
	curl "https://get.helm.sh/helm-$(HELM_VERSION)-linux-amd64.tar.gz" | tar zx; \
	sudo mv -f linux-amd64/helm /usr/local/bin/; \
	rm -rf linux-amd64; \
	fi; \
	fi

kubectl_dependencies: ## Utility target to install K8s dependencies
	@which kubectl ; rc=$$?; \
	if [ $$rc != 0 ]; then \
		sudo curl -L -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/$(KUBECTL_VERSION)/bin/linux/amd64/kubectl"; \
		sudo chmod +x /usr/bin/kubectl; \
	fi
	@printf "\nkubectl client version:"
	@kubectl version --client
	@printf "\nkubectl config view:"
	@kubectl config view
	@printf "\nkubectl config get-contexts:"
	@kubectl config get-contexts
	@printf "\nkubectl version:"
	@kubectl version

help:  ## show this help.
	@echo "$(MAKE) targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?\\= | ## "}; {printf "\033[36m%-30s\033[0m %-20s %-30s\n", $$1, $$2, $$3}'

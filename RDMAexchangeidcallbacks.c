// Copyright 2019 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * RDMAexchangeidcallbacks.c
 * Andrew Ensor
 * C API with range of callback function options for exchanging RDMA identifiers
 * PSN, QPN, GID, LID between local and remote
 *
*/

#include "RDMAexchangeidcallbacks.h"

/**********************************************************************
 * Log the resulting local and obtained remote identifiers
 **********************************************************************/
void logIdentifiers(
    uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
    uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr)
{
    logger(LOG_INFO, "local psn=%" PRIu32, packetSequenceNumber);
    logger(LOG_INFO, "local qpn=%" PRIu32, queuePairNumber);
    logger(LOG_INFO,
        "local gid=%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu",
        gidAddress.raw[0], gidAddress.raw[1], gidAddress.raw[2], gidAddress.raw[3],
        gidAddress.raw[4], gidAddress.raw[5], gidAddress.raw[6], gidAddress.raw[7],
        gidAddress.raw[8], gidAddress.raw[9], gidAddress.raw[10], gidAddress.raw[11],
        gidAddress.raw[12], gidAddress.raw[13], gidAddress.raw[14], gidAddress.raw[15]);
    logger(LOG_INFO, "local lid=%" PRIu16, localIdentifier);
    logger(LOG_INFO, "remote psn=%" PRIu32, *remotePSNPtr);
    logger(LOG_INFO, "remote qpn=%" PRIu32, *remoteQPNPtr);
    logger(LOG_INFO,
        "remote gid=%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu",
        remoteGIDPtr->raw[0], remoteGIDPtr->raw[1], remoteGIDPtr->raw[2], remoteGIDPtr->raw[3],
        remoteGIDPtr->raw[4], remoteGIDPtr->raw[5], remoteGIDPtr->raw[6], remoteGIDPtr->raw[7],
        remoteGIDPtr->raw[8], remoteGIDPtr->raw[9], remoteGIDPtr->raw[10], remoteGIDPtr->raw[11],
        remoteGIDPtr->raw[12], remoteGIDPtr->raw[13], remoteGIDPtr->raw[14], remoteGIDPtr->raw[15]);
    logger(LOG_INFO, "remote lid=%" PRIu16, *remoteLIDPtr);
}


/**********************************************************************
 * Exchanges the necessary RDMA configuration identifiers with those of
 * the remote RDMA peer, by printing the local values to the display
 * and prompting the user to enter the remote values
 * Returns EXCH_SUCCESS if identifier exchange has completed successfully
 **********************************************************************/
enum exchangeResult exchangeViaStdIO(bool isSendMode,
    uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
    uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr)
{
    char *remoteMode = (isSendMode ? "receiver" : "sender");
    printf("**********************************************************************\n");
    // loop to ensure receiver has necessary values before sender has them
    for (int i=0; i<2; i++)
    {
        if ((i==0 && isSendMode) || (i==1 && !isSendMode))
        {
            /* provide the RDMA identifiers via stdio */
            printf("Please pass to the remote %s: psn = %" PRIu32 "\n", remoteMode, packetSequenceNumber);
            printf("Please pass to the remote %s: qpn = %" PRIu32 "\n", remoteMode, queuePairNumber);
            printf("Please pass to the remote %s: "
                "gid = %hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu\n",
                remoteMode,
                gidAddress.raw[0], gidAddress.raw[1], gidAddress.raw[2], gidAddress.raw[3],
                gidAddress.raw[4], gidAddress.raw[5], gidAddress.raw[6], gidAddress.raw[7],
                gidAddress.raw[8], gidAddress.raw[9], gidAddress.raw[10], gidAddress.raw[11],
                gidAddress.raw[12], gidAddress.raw[13], gidAddress.raw[14], gidAddress.raw[15]);
            printf("Please pass to the remote %s: lid = %" PRIu16 "\n", remoteMode, localIdentifier);
        }
        else
        {
            /* request the RDMA identifiers via stdio */
            /* query user for remote RDMA information */
            printf("Please enter the remote %s packet sequence number (psn): ", remoteMode);
            int numInput = scanf("%" SCNu32, remotePSNPtr);
            printf("Please enter the remote %s queue pair number (qpn): ", remoteMode);
            numInput += scanf("%" SCNu32, remoteQPNPtr);
            printf("Please enter the remote %s global identifier as 16 bytes (gid): ", remoteMode);
            numInput += scanf("%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu",
                &remoteGIDPtr->raw[0], &remoteGIDPtr->raw[1], &remoteGIDPtr->raw[2], &remoteGIDPtr->raw[3],
                &remoteGIDPtr->raw[4], &remoteGIDPtr->raw[5], &remoteGIDPtr->raw[6], &remoteGIDPtr->raw[7],
                &remoteGIDPtr->raw[8], &remoteGIDPtr->raw[9], &remoteGIDPtr->raw[10], &remoteGIDPtr->raw[11],
                &remoteGIDPtr->raw[12], &remoteGIDPtr->raw[13], &remoteGIDPtr->raw[14], &remoteGIDPtr->raw[15]);
            printf("Please enter the remote %s local identifier (lid): ", remoteMode);
            numInput += scanf("%" SCNu16, remoteLIDPtr);
        }
    }
    printf("**********************************************************************\n");
    logIdentifiers(packetSequenceNumber, queuePairNumber, gidAddress, localIdentifier,
        remotePSNPtr, remoteQPNPtr, remoteGIDPtr, remoteLIDPtr);
    return EXCH_SUCCESS;
}


const char *identifierFileName;

/**********************************************************************
 * Sets the identifier file name for using exchangeViaSharedFiles
 **********************************************************************/
void setIdentifierFileName(const char *fileName)
{
    identifierFileName = fileName;
}


/**********************************************************************
 * Exchanges the necessary RDMA configuration identifiers with those of
 * the remote RDMA peer, by saving the local identifiers to a file and
 * reading the remote identifiers from another file
 * Note this function requires first setting the filename via setIdentifierFileName
 * Returns EXCH_SUCCESS if identifier exchange has completed successfully
 **********************************************************************/
enum exchangeResult exchangeViaSharedFiles(bool isSendMode,
    uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
    uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr)
{
    if (identifierFileName == NULL)
    {
        logger(LOG_ERR, "No identifier filename specified");
        return EXCH_MISSING_FILENAME;
    }
    // loop to ensure receiver has necessary values before sender has them
    for (int i=0; i<2; i++)
    {
        if ((i==0 && isSendMode) || (i==1 && !isSendMode))
        {
            /* provide the RDMA identifiers in a file */
            /* determine whether to suffix identifierFileName with ".send" or ".recv" */
            char fileNameWithSuffix[strlen(identifierFileName)+6];
            memset(fileNameWithSuffix, '\0', sizeof(fileNameWithSuffix));
            strcpy(fileNameWithSuffix, identifierFileName);
            if (isSendMode)
                strcat(fileNameWithSuffix, ".send");
            else /* is receive mode */
                strcat(fileNameWithSuffix, ".recv");

            FILE *filePointer;
            filePointer = fopen(fileNameWithSuffix, "w");
            fprintf(filePointer, "psn = %" PRIu32 "\n", packetSequenceNumber);
            fprintf(filePointer, "qpn = %" PRIu32 "\n", queuePairNumber);
            fprintf(filePointer,
                "gid = %hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu\n",
                gidAddress.raw[0], gidAddress.raw[1], gidAddress.raw[2], gidAddress.raw[3],
                gidAddress.raw[4], gidAddress.raw[5], gidAddress.raw[6], gidAddress.raw[7],
                gidAddress.raw[8], gidAddress.raw[9], gidAddress.raw[10], gidAddress.raw[11],
                gidAddress.raw[12], gidAddress.raw[13], gidAddress.raw[14], gidAddress.raw[15]);
            fprintf(filePointer, "lid = %" PRIu16 "\n", localIdentifier);
            fclose(filePointer);
        }
        else
        {
            /* request the RDMA identifiers from a file */
            /* determine whether to suffix identifierFileName with ".send" or ".recv" */
            char fileNameWithSuffix[strlen(identifierFileName)+6];
            memset(fileNameWithSuffix, '\0', sizeof(fileNameWithSuffix));
            strcpy(fileNameWithSuffix, identifierFileName);
            if (isSendMode)
                strcat(fileNameWithSuffix, ".recv");
            else /* is receive mode */
                strcat(fileNameWithSuffix, ".send");
            /* spin until the file exists */
            while (access(fileNameWithSuffix, F_OK|R_OK) != 0)
            {
                logger(LOG_WARNING, "Identifier file with name %s can not yet be read", fileNameWithSuffix);
                sleep(1); // sleep current thread for one second
            }
            FILE *filePointer;
            filePointer = fopen(fileNameWithSuffix, "r");
            int numInput = fscanf(filePointer, "psn = %" SCNu32 "\n", remotePSNPtr);
            numInput += fscanf(filePointer, "qpn = %" SCNu32 "\n", remoteQPNPtr);
            numInput += fscanf(filePointer,
                "gid = %hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu-%hhu\n",
                &remoteGIDPtr->raw[0], &remoteGIDPtr->raw[1], &remoteGIDPtr->raw[2], &remoteGIDPtr->raw[3],
                &remoteGIDPtr->raw[4], &remoteGIDPtr->raw[5], &remoteGIDPtr->raw[6], &remoteGIDPtr->raw[7],
                &remoteGIDPtr->raw[8], &remoteGIDPtr->raw[9], &remoteGIDPtr->raw[10], &remoteGIDPtr->raw[11],
                &remoteGIDPtr->raw[12], &remoteGIDPtr->raw[13], &remoteGIDPtr->raw[14], &remoteGIDPtr->raw[15]);
            numInput += fscanf(filePointer, "lid = %" SCNu16 "\n", remoteLIDPtr);
            fclose(filePointer);
        }
    }
    logIdentifiers(packetSequenceNumber, queuePairNumber, gidAddress, localIdentifier,
        remotePSNPtr, remoteQPNPtr, remoteGIDPtr, remoteLIDPtr);
    return EXCH_SUCCESS;
}

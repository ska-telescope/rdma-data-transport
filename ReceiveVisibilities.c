// Copyright 2019 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * ReceiveVisibilities.c
 * Andrew Ensor
 * C application that demonstrates sending and receiving SKA visibilities using RDMA UC
 * which accepts its configurations via command line arguments
 *
*/

#include "ReceiveVisibilities.h"

/**********************************************************************
 * Parses the command line arguments for server versus client mode
 **********************************************************************/
int parseCommandLineArguments(int argc, char *argv[], enum logType *requestLevel, enum runMode *mode,
    uint32_t *messageSize, uint32_t *numMemoryBlocks, uint32_t *numContiguousMessages, char **dataFileName, uint64_t *numTotalMessages,
    uint32_t *messageDelayTime, char **rdmaDeviceName, uint8_t *rdmaPort, int *gidIndex, char **identifierFileName,
    char **metricURL, uint32_t *numMetricAveraging)
{
    int opt;
    while ((opt = getopt(argc, argv, "l:m:b:c:f:t:d:r:p:g:x:o:v:s")) != -1)
    {
        switch (opt)
        {
            case 'l' :
                if (optarg != NULL)
                    *requestLevel = strtoul(optarg, NULL, 10);
                if (*requestLevel < LOG_EMERG)
                    *requestLevel = LOG_EMERG;
                else if (*requestLevel > LOG_DEBUG)
                    *requestLevel = LOG_DEBUG;
                logger(LOG_DEBUG, "Command line argument: log level %d", *requestLevel);
                break;
            case 'm' :
                if (optarg != NULL)
                    *messageSize = strtoul(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: messageSize %" PRIu32, *messageSize);
                break;
            case 'b' :
                if (optarg != NULL)
                    *numMemoryBlocks = strtoul(optarg, NULL, 10);
                if (*numMemoryBlocks < 1)
                    *numMemoryBlocks = 1;
                logger(LOG_DEBUG, "Command line argument: numMemoryBlocks %" PRIu32, *numMemoryBlocks);
                break;
            case 'c' :
                if (optarg != NULL)
                    *numContiguousMessages = strtoul(optarg, NULL, 10);
                if (*numContiguousMessages < 1)
                    *numContiguousMessages = 1;
                logger(LOG_DEBUG, "Command line argument: numContiguousMessages %" PRIu32, *numContiguousMessages);
                break;
            case 'f' :
                if (optarg != NULL)
                    *dataFileName = optarg;
                logger(LOG_DEBUG, "Command line argument: dataFileName %s", *dataFileName);
                break;
            case 't' :
                if (optarg != NULL)
                    *numTotalMessages = strtoull(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: numTotalMessages %" PRIu64, *numTotalMessages);
                break;
            case 'd' :
                if (optarg != NULL)
                    *messageDelayTime = strtoul(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: messageDelayTime %" PRIu32, *messageDelayTime);
                break;
            case 'r' :
                if (optarg != NULL)
                    *rdmaDeviceName = optarg;
                logger(LOG_DEBUG, "Command line argument: rdmaDevice %s", *rdmaDeviceName);
                break;
            case 'p' :
                if (optarg != NULL)
                    *rdmaPort = strtoul(optarg, NULL, 10);
                if (*rdmaPort < 1)
                    *rdmaPort = 1;
                else if (*rdmaPort > 2)
                    *rdmaPort = 2;
                logger(LOG_DEBUG, "Command line argument: rdmaPort %d", *rdmaPort);
                break;
            case 'g' :
                if (optarg != NULL)
                    *gidIndex = strtol(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: gidIndex %d", *gidIndex);
                break;
            case 'x' :
                if (optarg != NULL)
                    *identifierFileName = optarg;
                logger(LOG_DEBUG, "Command line argument: identifierFileName %s", *identifierFileName);
                break;
            case 'o' :
                if (optarg != NULL)
                    *metricURL = optarg;
                logger(LOG_DEBUG, "Command line argument: metricURL %s", *metricURL);
                break;
            case 'v' :
                if (optarg != NULL)
                    *numMetricAveraging = strtoul(optarg, NULL, 10);
                if (*numMetricAveraging < 1)
                    *numMetricAveraging = 1;
                logger(LOG_DEBUG, "Command line argument: numMetricAveraging %" PRIu32, *numMetricAveraging);
                break;
            case 's' :
                logger(LOG_DEBUG, "Command line argument: send mode");
                *mode = SEND_MODE;
                break;
            default :
                logger(LOG_CRIT, "Usage: %s [-l log level 0..6]"
                    "[-m message size in bytes] [-b num memory blocks] [-c num contig messages per block] "
                    "[-f data filename] [-t total num messages] [-d delay microsec time per message] "
                    "[-r rdma device name] [-p device port] [-g gid index] [-x exchange identifier filename] "
                    "[-o metric URL] [-v metric averaging] [-s]",
                    argv[0]);
                return FAILURE;
        }
    }

    return SUCCESS;
}


/**********************************************************************
 * Main method to execute
 **********************************************************************/
int main(int argc, char *argv[])
{
    /* set up default values that might be overriden by command line arguments */
    enum logType requestLogLevel = LOG_NOTICE;
    enum runMode mode = RECV_MODE;
    uint32_t messageSize = 65536; /* size in bytes of single RDMA message */
    uint32_t numMemoryBlocks = 1; /* number of memory blocks to allocate for RDMA messages */
    uint32_t numContiguousMessages = 1; /* number of contiguous messages to hold in each memory block */
    char *dataFileName = NULL; /* default to not loading (sender) nor saving (receiver) to file the data memory blocks */
    uint64_t numTotalMessages = 0; /* total number of messages to send or receive, if 0 then default to numMemoryBlocks*numContiguousMessages */
    uint32_t messageDelayTime = 0; /* time in milliseconds to delay after each message send/receive posted, default is no delay */
    char *rdmaDeviceName = NULL; /* no preferred rdma device name to choose */
    uint8_t rdmaPort = 1;
    int gidIndex = -1; /* preferred gid index or -1 for no preference */
    char *identifierFileName = NULL; /* default to using stdio for exchanging RDMA identifiers */
    char *metricURL = NULL; /* default to not push metrics */
    uint32_t numMetricAveraging = 0; /* number of message completions over which to average metrics, default to numMemoryBlocks*numContiguousMessages */

    /* parse command line arguments to override the above defaults */
    if (parseCommandLineArguments(argc, argv, &requestLogLevel, &mode,
        &messageSize, &numMemoryBlocks, &numContiguousMessages,
        &dataFileName, &numTotalMessages, &messageDelayTime,
        &rdmaDeviceName, &rdmaPort, &gidIndex, &identifierFileName, &metricURL, &numMetricAveraging)
        != SUCCESS)
    {
        logger(LOG_CRIT, "Error while passing command line arguments");
        return FAILURE;
    }
    if (numTotalMessages == 0)
        numTotalMessages = numMemoryBlocks * numContiguousMessages;
    if (numMetricAveraging == 0)
        numMetricAveraging = numMemoryBlocks * numContiguousMessages;
    setLogLevel(requestLogLevel);

    if (mode == SEND_MODE)
    {
        logger(LOG_INFO, "Receive Visibilities starting as sender");
    }
    else
    {
        logger(LOG_INFO, "Receive Visibilities starting as receiver");
    }

    /* allocate memory blocks as buffers to be used in the memory regions */
    uint64_t memoryBlockSize = messageSize * numContiguousMessages;
    void **memoryBlocks = allocateMemoryBlocks(memoryBlockSize, &numMemoryBlocks);

    /* create a memory region manager to manage the usage of the memory blocks */
    /* note for simplicity memory region manager doesn't monitor memory regions so this code doesn't have to */
    /* load memory blocks with new data during sending/receiving */
    MemoryRegionManager* manager = createMemoryRegionManager(memoryBlocks,
        messageSize, numMemoryBlocks, numContiguousMessages, numTotalMessages, false);

    /* if in send mode then populate the memory blocks and display contents to stdio */
    if (mode == SEND_MODE)
    {
        if (dataFileName != NULL)
        {
            // read data from dataFileName.0, dataFileName.1, ... into memory blocks
            if (!readFilesIntoMemoryBlocks(manager, dataFileName))
            {
                logger(LOG_WARNING, "Unsuccessful read of data files");
            }
        }
        else
        {
            /* put something into each allocated memory block to test sending */
            for (uint32_t blockIndex=0; blockIndex<numMemoryBlocks; blockIndex++)
            {
                for (uint64_t i=0; i<memoryBlockSize; i++)
                {
                    ((unsigned char*)memoryBlocks[blockIndex])[i]
                        = (unsigned char)(i+(blockIndex*memoryBlockSize));
                }
            }
        }
        setAllMemoryRegionsPopulated(manager, true);
        displayMemoryBlocks(manager, 10, 10); /* display contents that will send */
    }
    else
    {
        /* clear each memory block */
        for (uint32_t blockIndex=0; blockIndex<numMemoryBlocks; blockIndex++)
        {
            memset(memoryBlocks[blockIndex], 0, memoryBlockSize);
        }
        setAllMemoryRegionsPopulated(manager, false);
    }

    /* create a pointer to an identifier exchange function (see eg RDMAexchangeidentifiers for some examples) */
    enum exchangeResult (*identifierExchangeFunction)(bool isSendMode,
        uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
        uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr) = NULL;
    if (identifierFileName != NULL)
    {
        setIdentifierFileName(identifierFileName);
        identifierExchangeFunction = exchangeViaSharedFiles;
    }

    /* prepare the RDMA device and then perform RDMA transfers in a separate thread */
    rdmaTransferMemoryBlocks(mode, manager,
        messageDelayTime, identifierExchangeFunction,
        rdmaDeviceName, rdmaPort, gidIndex,
        metricURL, numMetricAveraging);
    /* process the message data as they get transferred */
    uint64_t expectedTransfer = 0;
    uint32_t currentRegionIndex = 0;
    uint32_t currentContiguousIndex = 0;
    while (expectedTransfer < numTotalMessages)
    {
        /* delay until the current memory location has its data transferred */
        uint64_t currentTransfer = getMessageTransferred(manager, currentRegionIndex, currentContiguousIndex);
        while (currentTransfer==NO_MESSAGE_ORDINAL || currentTransfer<expectedTransfer)
        {
            /* expected message transfer has not yet taken place */
            microSleep(10); /* sleep current thread before checking currentTransfer again */
            currentTransfer = getMessageTransferred(manager, currentRegionIndex, currentContiguousIndex);
        }
        /* process the current message transfer */
        if (currentTransfer > expectedTransfer)
        {
            /* messages between expectedTransfer and currentTransfer-1 inclusive have been dropped during the transfer */
            /* HERE: do something about the missing messages */
        }
        /* note message data for currentTransfer is now available in memoryBlocks[currentRegionIndex] at start index currentContiguousIndex*messageSize */
        /* HERE: do some processing with the current message transfer */
        /* once done processing the current message transfer move along to the next memory location */
        expectedTransfer = currentTransfer + 1;
        currentContiguousIndex++;
        if (currentContiguousIndex >= manager->numContiguousMessages)
        {
            /* move along to the start of the next memory region */
            setMemoryRegionPopulated(manager, currentRegionIndex, false); /* finished processing this memory region so it can now be reused */
            currentContiguousIndex = 0;
            currentRegionIndex++;
            if (currentRegionIndex >= manager->numMemoryRegions)
            {
                /* wrap around to reuse the same memory regions */
                currentRegionIndex = 0;
            }
        }
    }
    /* ensure the RDMA transfers taking place in a separate thread have completed by making current thread wait until they have completed */
    waitForRdmaTransferCompletion(manager); 

    /* if in receive mode then display contents to stdio */
    if (mode == RECV_MODE)
    {
        if (dataFileName != NULL)
        {
            // write data to dataFileName.0, dataFileName.1, ... from memory blocks
            if (!writeFilesFromMemoryBlocks(manager, dataFileName))
            {
                logger(LOG_WARNING, "Unsuccessful write of data files");
            }
        }
        displayMemoryBlocks(manager, 10, 10); /* display contents that were received */
    }

    destroyMemoryRegionManager(manager);
    /* free memory block buffers */
    freeMemoryBlocks(memoryBlocks, numMemoryBlocks);

    logger(LOG_INFO, "Receive Visibilities ending");
    return 0;
}
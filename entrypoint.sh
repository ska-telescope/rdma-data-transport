#!/usr/bin/env bash
set -Eeo pipefail
# TODO swap to -Eeuo pipefail above (after handling all potentially-unset variables)

# kickoff
if [ "$1" = 'receive' ]; then
	# launch app
	cd /app
	exec ./receive "$@"
fi

exec "$@"

ARG BASE_BUILD_IMAGE=ubuntu:18.04
ARG RUNTIME_IMAGE=ubuntu:18.04

# vanilla Ubuntu image used as builder
FROM $BASE_BUILD_IMAGE AS build

LABEL \
      author="Piers Harding <piers@ompka.net>" \
      description="rdma-data-transport" \
      license="Apache2.0" \
      registry="library/piersharding/rdma-data-transport" \
      vendor="None" \
      org.skatelescope.team="NZAPP" \
      org.skatelescope.version="0.0.1" \
      org.skatelescope.website="http://gitlab.com/ska-telescope/rdma-data-transport/"

# Disable prompts from apt.
ENV DEBIAN_FRONTEND noninteractive

# see: https://docs.mellanox.com/pages/releaseview.action?pageId=15049785

# install generic dependencies for gcc build
RUN apt-get update && \
    apt-get  -yq --no-install-recommends install \
      gcc \
      git \
      libc6-dev \
      make \
      && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*

# now install dependencies for RDMA build
RUN apt-get update && \
    apt-get  -yq --no-install-recommends install \
      libibmad5 \
      libibumad3 \
      libibumad-dev \
      librdmacm1 \
      librdmacm-dev \
      rdma-core \
      rdmacm-utils \
      ibutils \
      ibverbs-providers \
      && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*

RUN \
    mkdir -p /app

# copy over project ready for build
COPY *.c *.h Makefile .git /app/

# build rdma transport application
RUN \
    cd /app && make build

# Now - create a clean image without build environment
FROM $RUNTIME_IMAGE

# Disable prompts from apt.
ENV DEBIAN_FRONTEND noninteractive

# now install dependencies for RDMA runtime
RUN apt-get update && \
    apt-get  -yq --no-install-recommends install \
      libibmad5 \
      libibumad3 \
      librdmacm1 \
      ibutils \
      ibverbs-providers \
      rdma-core \
      rdmacm-utils \
      && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*

# copy in built rdma transport app
COPY --from=build /app/receive /app/receive

WORKDIR /app

# copy in boot strap shellscript that does ldconfig and runs rdma transport
COPY entrypoint.sh /entrypoint.sh

# Setup the entrypoint or environment
ENTRYPOINT ["/entrypoint.sh"]

# Run - default is receive
CMD ["receive"]

# vim:set ft=dockerfile:

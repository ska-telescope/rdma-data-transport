// Copyright 2019 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * RDMAmetricreporter.c
 * Andrew Ensor
 * C API for handling metrics reported when using RDMA
 *
*/

#include "RDMAmetricreporter.h"

/**********************************************************************
 * Initialise the (libcurl) reporter of metrics
 **********************************************************************/
void initialiseMetricReporter()
{
    curl_global_init(CURL_GLOBAL_ALL);
}

/**********************************************************************
 * Cleanup the (libcurl) reporter of metrics
 **********************************************************************/
void cleanupMetricReporter()
{
    curl_global_cleanup();
}

/**********************************************************************
 * struct that holds a single POST request of metrics
 **********************************************************************/
struct httpPostParams
{
    pthread_t *metricThread;
    char *metricSource;
    char *metricURL;
    int queueUtilisation;
    int cpuUtilisation;
    double bandwidth;
    uint64_t metricMessagesTransferred;
    uint64_t metricWorkRequestsMissing;
    uint64_t millisecondsSinceEpoch;
    MemoryRegionManager *manager;
};

/**********************************************************************
 * Pushes a single POST request of metrics
 * Note this function should free the struct httpPostParams when done
 **********************************************************************/
static void *httpPostThread(void *ptr)
{
    struct httpPostParams *params = (struct httpPostParams *)ptr;
    if (params->manager == NULL)
    {
        // memory manager has been destroyed so don't push this late metric
        logger(LOG_INFO, "Metric dropped as memory manager has been destroyed");
        return NULL;
    }

    /* set the header information for InfluxDB */
    char postRequestHeader[strlen(params->metricURL)+46];
    memset(postRequestHeader, '\0', sizeof(postRequestHeader));
    strcat(postRequestHeader, "http://");
    strcat(postRequestHeader, params->metricURL);
    strcat(postRequestHeader, ":8086/write?db=vismetrics&precision=ms");

    const uint utilMetricsSize = strlen(params->metricSource)+114;
    const uint regionMetricsSize = params->manager->numMemoryRegions * (strlen(params->metricSource)+50);
    char postRequestMetrics[utilMetricsSize + regionMetricsSize];
    memset(postRequestMetrics, '\0', sizeof(postRequestMetrics));

    bool populatedRegions[params->manager->numMemoryRegions];
    memset(populatedRegions, 0, sizeof(populatedRegions));
    getAllMemoryRegionsPopulated(params->manager, populatedRegions);
    bool enqueuedRegions[params->manager->numMemoryRegions];
    memset(enqueuedRegions, 0, sizeof(enqueuedRegions));
    getAllMemoryRegionsEnqueued(params->manager, enqueuedRegions);

    int size = sprintf(postRequestMetrics,
        "%s queueutil=%d,cpuutil=%d,bandwidth=%.2f,msgTrans=%" PRIu64 ",msgMiss=%" PRIu64 " %" PRIu64,
        params->metricSource, params->queueUtilisation, params->cpuUtilisation, params->bandwidth,
        params->metricMessagesTransferred, params->metricWorkRequestsMissing,
        params->millisecondsSinceEpoch);
    for (uint32_t regionIndex=0; regionIndex<params->manager->numMemoryRegions; regionIndex++)
    {
        // note using 0,1 instead of false,true as grafana not graphing boolean values by default
        size += sprintf(&postRequestMetrics[size],
            "\n%s,reg=%07d pop=%s,enq=%s %" PRIu64,
            params->metricSource, regionIndex,
            (populatedRegions[regionIndex]==true?"1":"0"),
            (enqueuedRegions[regionIndex]==true?"1":"0"),
            params->millisecondsSinceEpoch);
    }
    CURL *curl;
    CURLcode res;
    curl = curl_easy_init();
    if (curl != NULL)
    {
        curl_easy_setopt(curl, CURLOPT_URL, postRequestHeader);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postRequestMetrics);
        res = curl_easy_perform(curl);
        if (res != CURLE_OK)
        {
            logger(LOG_WARNING, "Unable to post metrics via HTTP with curl error %s\n", curl_easy_strerror(res));
        }
        curl_easy_cleanup(curl);
    }
    free(params->metricThread); // free malloc in pushMetrics
    free(params); // free malloc in pushMetrics
    return NULL;
}

/**********************************************************************
 * Pushes the calculated metrics via an HTTP POST request
 **********************************************************************/
void pushMetrics(char *metricSource, char *metricURL,
    int queueUtilisation, int cpuUtilisation, double bandwidth,
    uint64_t metricMessagesTransferred, uint64_t metricWorkRequestsMissing,
    uint64_t millisecondsSinceEpoch,
    MemoryRegionManager *manager)
{
    pthread_t *metricThread = NULL;
    metricThread = (pthread_t*)malloc(sizeof(pthread_t)); // freed in httpPostThread
    pthread_attr_t pthreadAttributes;
    pthread_attr_init(&pthreadAttributes);
    pthread_attr_setstacksize(&pthreadAttributes, 65536); // 64k stack allocation for pthread
    pthread_attr_setdetachstate(&pthreadAttributes, PTHREAD_CREATE_DETACHED); // pthread will not be joinable

    struct httpPostParams *params = NULL;
    params = (struct httpPostParams*)malloc(sizeof(struct httpPostParams)); // freed in httpPostThread
    params->metricThread = metricThread;
    params->metricSource = metricSource;
    params->metricURL = metricURL;
    params->queueUtilisation = queueUtilisation;
    params->cpuUtilisation = cpuUtilisation;
    params->bandwidth = bandwidth;
    params->metricMessagesTransferred = metricMessagesTransferred;
    params->metricWorkRequestsMissing = metricWorkRequestsMissing;
    params->millisecondsSinceEpoch = millisecondsSinceEpoch;
    params->manager = manager;
    if (pthread_create(metricThread, &pthreadAttributes, httpPostThread, (void*)params) != 0)
        logger(LOG_ERR, "Unable to create thread for posting metrics via HTTP");
}

// Copyright 2019 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * RDMAmetricreporter.h
 * Andrew Ensor
 * C API for handling metrics reported when using RDMA
 *
*/

#ifndef RDMA_METRIC_REPORTER_H
#define RDMA_METRIC_REPORTER_H

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <curl/curl.h>
#include <pthread.h>

#include "RDMAlogger.h"
#include "RDMAmemorymanager.h"


/**********************************************************************
 * Initialise the (libcurl) reporter of metrics
 **********************************************************************/
void initialiseMetricReporter();

/**********************************************************************
 * Cleanup the (libcurl) reporter of metrics
 **********************************************************************/
void cleanupMetricReporter();

/**********************************************************************
 * Pushes the calculated metrics via an HTTP POST request
 **********************************************************************/
void pushMetrics(char *metricSource, char *metricURL,
    int queueUtilisation, int cpuUtilisation, double bandwidth,
    uint64_t metricMessagesTransferred, uint64_t metricWorkRequestsMissing,
    uint64_t millisecondsSinceEpoch,
    MemoryRegionManager *manager);

#endif
